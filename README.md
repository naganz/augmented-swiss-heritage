# Augmented Swiss Heritage

## Description
The app "Augmented Swiss Heritage" allows visitors to experience the cultural heritage of Davos in an immersive and participatory way. Along three thematic areas - art and culture, health and sports - nearly forty sights are enriched with augmented reality. Four geographically simple routes lead through Davos and its surroundings and take the app users on a journey into the past. The sights are augmented in different ways, some are interactive in the form of simple interactions, like small puzzles. Each sight is accompanied by a text and an audio track. A sight can also be viewed at home, each artifact can be placed and viewed by hand in your own home. However, the better experience is obtained in the context of Davos itself.

This app was developed by University of Applied Sciences of the Grisons (FHGR) and afca. ag, in collaboration with the Kirchner Museum and the Heimatmuseum Davos. It is a research project, which was funded by Innotour and Kreativfonds Davos. 

A detailed description of the project can be found here: [Augmented Swiss Heritage](https://augmentedswissheritage.fhgr.ch/)

To get an impression of the app, watch this video: [YouTube](https://www.youtube.com/watch?v=J3tr-jSbbTw&t=2s)

## Installation
The app was implemented in Unity 2020.3.21f1. For hosting the AR Models and Anchors, we used Microsoft Azure Spatial Anchors. You can clone this repo and build it in Unity. The app is also available for download in the App Stores: 
[Apple App Store](https://apps.apple.com/ch/app/augmented-swiss-heritage/id1601190019), 
[Google Play Store](https://play.google.com/store/apps/details?id=com.afca.ag.ASHContentTester&gl=CH) 

The project originally contained Assets from the Unity Asset Store. When you open the Unity project, errors will occur. To fix this, you have to get the following assets from the Asset Store:
* Mapbox
* Hamburger Menu
* Realistic Effects Pack
* Magnetic Scroll View
* Mute Switch Detector
* Native Share
* Paint in 3D
* Sprite Shaders Ultimate
* Wolfulus

## Authors and acknowledgment
The source code was developed by FHGR and afca. ag. 

## Contact
If you have questions or want to develope a similar AR app, feel free to contact Nadine Ganz (FHGR):
nadine.ganz@fhgr.ch

## License
The project is licensed under the MIT License
