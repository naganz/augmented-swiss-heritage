using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using System;
using System.Timers;
using System.Threading.Tasks;

public class AppInsights : MonoBehaviour
{
    [SerializeField]
    string ConnectionString;

    public static AppInsights Instance { get; private set; }

    TelemetryClient telemetry;


    #region Unity Lifecycle
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        if (Application.isEditor)
        {
            Debug.Log("No Tracking in Editor");
        }
        else
        {
            TelemetryConfiguration configuration = TelemetryConfiguration.CreateDefault();
            configuration.ConnectionString = ConnectionString;
            telemetry = new TelemetryClient(configuration);
            telemetry.Context.Session.Id = Guid.NewGuid().ToString();
            telemetry.Context.Component.Version = Application.version;
            telemetry.Context.Device.Type = Application.platform.ToString();
            telemetry.TrackEvent("App start", properties: addProperties(null));
        }
        Instance = this;
    }

     private void OnApplicationPause()
    {
        if (telemetry != null)
        {
            telemetry.Flush();
        }
    }
    #endregion

    #region Track Methods
    
    IDictionary<string,string> addProperties(IDictionary<string, string> properties)
    {
        if (properties == null) properties = new Dictionary<string,string>();
        if (Input.location.status == LocationServiceStatus.Running)
        {
            properties.Add("GpsHorizontalAccuracy", Input.location.lastData.horizontalAccuracy.ToString());
            properties.Add("GpsVerticalAccuracy", Input.location.lastData.verticalAccuracy.ToString());
            properties.Add("Latitude", Input.location.lastData.latitude.ToString());
            properties.Add("Longitude", Input.location.lastData.longitude.ToString());
        }
        return properties;

    }
    /// <summary>
    /// Tracks a Information (Level Information)
    /// </summary>
    /// <param name="text">Text to be writen in the Telemetry</param>
    public void TrackTrace(string text)
    {
        if (telemetry != null) telemetry.TrackTrace(text,addProperties(null));
    }
    /// <summary>
    /// Tracks a Information with a Log Level
    /// </summary>
    /// <param name="text">Text to be writen in the Telemetry</param>
    /// <param name="level">Log Level (for example Warning)</param>
    public void TrackTrace(string text, SeverityLevel level)
    {
        if (telemetry != null) telemetry.TrackTrace(text, level,addProperties(null));
    }
    /// <summary>
    /// Tracks a Page or Screen view
    /// </summary>
    /// <param name="name">Name of the Page or View</param>
    /// <param name="duration">Duration the View is active</param>
    public void TrackPageView(string name, TimeSpan duration)
    {
        if (telemetry != null)
        {
            var pageView = new PageViewTelemetry(name) { Duration = duration };
            addProperties(pageView.Properties);
            telemetry.TrackPageView(pageView);
        }
    }
    /// <summary>
    /// Tracks a Exception on the Client, please ad to your Exception Handling
    /// </summary>
    /// <param name="ex">Exception</param>
    public void TrackException(Exception ex)
    {
        if (telemetry != null) telemetry.TrackException(ex,addProperties(null));
    }
    /// <summary>
    /// Tracks an Event like Klick on a Button or Filter a List
    /// </summary>
    /// <param name="eventName">Event Name</param>
    public void TrackEvent(string eventName)
    {
        if (telemetry != null) telemetry.TrackEvent(eventName, properties: addProperties(null));

    }
    #endregion
}