using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ChangeButtonIcon : MonoBehaviour
{
    Image buttonImage;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        buttonImage = GetComponent<Image>();
    }

    public void ChangeIcon(Sprite newIcon)
    {
        buttonImage.sprite = newIcon;
    }


    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnChangeButtonIcon += ChangeIcon;
    }

    void UnRegisterEvents()
    {
        EventManager.OnChangeButtonIcon -= ChangeIcon;
    }
    #endregion
}
