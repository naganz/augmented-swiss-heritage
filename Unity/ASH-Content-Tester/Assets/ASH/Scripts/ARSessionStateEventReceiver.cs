using ASH.Structures;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARSessionStateEventReceiver : MonoBehaviour
{
    [SerializeField]
    ARSession arSession;

    PointOfInterest poi;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

  
    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnOpenARAction += EventManager_OnOpenARAction;
        EventManager.OnCloseARAction += EventManager_OnCloseARAction;
    }

    void UnRegisterEvents()
    {
        EventManager.OnOpenARAction -= EventManager_OnOpenARAction;
        EventManager.OnCloseARAction -= EventManager_OnCloseARAction;
    }
    #endregion

    #region Event Handlers
    private void EventManager_OnOpenARAction(PointOfInterest pointOfInterest)
    {
        poi = pointOfInterest;
        arSession.enabled = true;
        EventManager.SendStartAsaSession(this.poi);
    }
   
    private void EventManager_OnCloseARAction()
    {
        arSession.enabled = false;
    }
    #endregion
}
