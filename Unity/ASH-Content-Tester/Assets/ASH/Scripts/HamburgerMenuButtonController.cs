using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HamburgerMenuButtonController : MonoBehaviour
{

    #region Inspector Properties

    [Header("Config Values")]
    [SerializeField]
    private EHamburgerMenuState menuState;
    #endregion

    #region Public Functions

    public void HandleClick()
    {
        EventManager.HamburgerMenuActiveState = this.menuState;
    }
    #endregion
}

