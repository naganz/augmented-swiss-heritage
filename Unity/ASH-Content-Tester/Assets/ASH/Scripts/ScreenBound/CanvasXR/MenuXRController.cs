using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MenuXRController : MonoBehaviour
{

    [SerializeField]
    GameObject ButtonSchliessen;
    [SerializeField]
    GameObject ButtonRefresh;
    [SerializeField]
    GameObject ButtonPannel_Bottom;

    #region private Properties
    #endregion

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    #region Register/Unregister Events
    void RegisterEvents()
    {
     //   EventManager.OnAnchorFound += EventManager_OnAnchorFound;
    }

    void UnRegisterEvents()
    {
    //    EventManager.OnAnchorFound -= EventManager_OnAnchorFound;
    }
    #endregion

    #region Event Handlers
    private void EventManager_OnAnchorFound()
    {
        throw new System.NotImplementedException();
    }
    #endregion

    #region Public Functions

    public void CloseARClick()
    {
        EventManager.SendCloseARAction();
    }
    public void RefreshARClick()
    {
        EventManager.SendRefreshAR();

    }
    public void ToggleMenuPlay()
    {
        EventManager.SendToggleVideoAudio();
    }

    public void ToggleMenuPlace()
    {
        EventManager.SendToggle3DPlaceAR();
    }

    public void ButtonPictureClick()
    {
        // Ausblenden der Schaltfl?chen funktioniert (noch) nicht
        setButtonActive(false);
        ScreenCapture.CaptureScreenshot("ash-screen.png");
        NativeShare share = new NativeShare();
        share.SetSubject("Augmented Swiss Heritage");
        share.AddFile($"{Application.persistentDataPath}/ash-screen.png");
        share.SetTitle("Augmented Swiss Heritage");
        share.Share();
        setButtonActive(true);
        
    }
    #endregion

    #region Visibility Helper
    void setButtonActive(bool isActive)
    {
        ButtonPannel_Bottom.SetActive(isActive);
        ButtonRefresh.SetActive(isActive);
        ButtonSchliessen.SetActive(isActive);
    }
    #endregion


}
