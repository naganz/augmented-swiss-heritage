using AnchorContent.Structures;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasXRController : MonoBehaviour
{
    [SerializeField]
    GameObject MenuXR;
    [SerializeField]
    GameObject ArIntroduction;
    [SerializeField]
    GameObject ScanASAPanel;
    [SerializeField]
    GameObject ManualPlacePanel;
    [SerializeField]
    GameObject CloseXR;
    [SerializeField]
    ScreenBound_CanvasController canvasController;
    [SerializeField]
    GameObject AudioIsMute;

    bool isIntroductionOkayPressed;
    bool isAnchorFound;
    ContentPlaced content;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
        isAnchorFound = false;
        ManualPlacePanel.SetActive(false);
        CloseXR.SetActive(true);
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnSpawnModel += EventManager_OnSpawnModel;
        EventManager.OnManualPlacement += EventManager_OnManualPlacement;
        EventManager.OnDisplayWarningSound += EventManager_OnDisplayWarningSound;
    }

    void UnRegisterEvents()
    {
        EventManager.OnSpawnModel -= EventManager_OnSpawnModel;
        EventManager.OnManualPlacement -= EventManager_OnManualPlacement;
        EventManager.OnDisplayWarningSound -= EventManager_OnDisplayWarningSound;
    }
    #endregion

    #region Event Handlers
    private void EventManager_OnSpawnModel(GameObject anchor, ContentPlaced place)
    {
        content = place;
        ScanASAPanel.SetActive(false);
        ManualPlacePanel.SetActive(false);
        MenuXR.SetActive(true);
        CloseXR.SetActive(false);
        isAnchorFound = true;
    }

    private void EventManager_OnDisplayWarningSound(bool val)
    {
        AudioIsMute.SetActive(val);
    }

    #endregion

    #region Input Handlers
    public void ARIntroductionButtonClick()
    {
        isIntroductionOkayPressed = true;
        ArIntroduction.SetActive(false);
        if (!isAnchorFound)
        {
            ScanASAPanel.SetActive(true);
        }
    }
    public void PlaceManualClick()
    {
        ScanASAPanel.SetActive(false);
        ManualPlacePanel.SetActive(true);
        EventManager.SendStartSpawnPrefab(this.canvasController.OpenPointOfInterest.PrefabName);
        MenuXR.SetActive(true);
        CloseXR.SetActive(false);
    }
    private void EventManager_OnManualPlacement()
    {
        ManualPlacePanel.SetActive(false);
    }
    #endregion
}
