using ASH.Structures;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AR_IntroductionController : MonoBehaviour
{
    [SerializeField]
    ScreenBound_CanvasController canvasController;

    [SerializeField]
    TextMeshProUGUI textName;

    [SerializeField]
    Text textXrIntro;

    //PointOfInterest poi;

    [SerializeField]
    private Image image;

    AsyncOperationHandle<Sprite> handle;
    bool doUpdatePoi;

    private void OnEnable()
    {
        doUpdatePoi = true;
    }
    private void OnDisable()
    {
        this.image.sprite = null;
    }

    private void Update()
    {
        if (doUpdatePoi)
        {
            var poi = canvasController.OpenPointOfInterest;
            textName.text = poi.Name;
            textXrIntro.text = poi.XrIntroText;

            string addressableName = poi.XrIntroImage;

            var aoh = Addressables.LoadAssetAsync<Sprite>(poi.XrIntroImage);
            aoh.Completed += (handler) => {
                this.image.sprite = handler.Result;
            };
                       
            doUpdatePoi = false;
        }
    }

  
}
