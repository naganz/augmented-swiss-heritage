using ASH.Structures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ScreenBound_CanvasController : MonoBehaviour
{
    #region Inspector Properties

    [SerializeField]
    private GameObject NonArCanvas;

    [SerializeField]
    private GameObject ArCanvas;

    [SerializeField]
    private GameObject MapRoot;

    [SerializeField]
    private bool autorotateNonArCanvas;

    [SerializeField]
    private bool autorotateArCanvas;

    #endregion

    #region public properties
    public PointOfInterest OpenPointOfInterest { get { return poi; } }
    #endregion

    #region private Properties
    PointOfInterest poi;
    DateTime openTime;
    #endregion

    #region Framework Functions

    private void Start()
    {
        NonArCanvas.SetActive(true);
        ArCanvas.SetActive(false);
        MapRoot.SetActive(true);
        setAutoRotation(autorotateNonArCanvas);
    }
    void OnEnable()
    {

        this.subscribeToEvents();

    }

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events
    private void subscribeToEvents()
    {
        EventManager.OnOpenARAction += EventManager_OnOpenARAction;
        EventManager.OnCloseARAction += EventManager_OnCloseARAction;
        EventManager.OnRefreshAR += EventManager_OnRefreshAR;
    }
    private void unsubscribeFromEvents()
    {
        EventManager.OnOpenARAction -= EventManager_OnOpenARAction; ;
        EventManager.OnCloseARAction -= EventManager_OnCloseARAction;
        EventManager.OnRefreshAR -= EventManager_OnRefreshAR;
    }
    private void EventManager_OnOpenARAction(ASH.Structures.PointOfInterest pointOfInterest)
    {
        AppInsights.Instance.TrackEvent($"Open Point of Interest: '{pointOfInterest.Name}'");
        openTime = DateTime.Now;
        poi = pointOfInterest;
        NonArCanvas.SetActive(false);
        ArCanvas.SetActive(true);
        MapRoot.SetActive(false);
        setAutoRotation(autorotateArCanvas);
    }

    private void EventManager_OnCloseARAction()
    {
        var duration = DateTime.Now - openTime;
        AppInsights.Instance.TrackPageView(poi.Name, duration);
        NonArCanvas.SetActive(true);
        ArCanvas.SetActive(false);
        MapRoot.SetActive(true);
        setAutoRotation(autorotateNonArCanvas);
    }
    private void EventManager_OnRefreshAR()
    {
        EventManager.SendCloseARAction();
        StartCoroutine(reopenAR());
    }
    private IEnumerator reopenAR()
    {
        yield return new WaitForSeconds(0.1f);

        EventManager.SendOpenARAction(poi);

    }
    #endregion

    #region Private Functions

    private void setAutoRotation(bool enable)
    {

        Screen.autorotateToLandscapeLeft = enable;
        Screen.autorotateToLandscapeRight = enable;
        //Screen.autorotateToPortraitUpsideDown = false;
        if (!enable)
        {
            Screen.orientation = ScreenOrientation.Portrait;
        }
        else
        {
            Screen.orientation = ScreenOrientation.AutoRotation;
        }
    }

    #endregion
}
