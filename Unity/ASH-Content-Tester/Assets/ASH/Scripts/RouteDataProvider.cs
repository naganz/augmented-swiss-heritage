using ASH.Structures;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouteDataProvider : MonoBehaviour
{

    #region Inspector Properties

    [Header("Config Values")]
    [SerializeField]
    private string routeListURL = "";

    [SerializeField]
    private string poiListURL = "";

    #endregion

    #region Public Properties

    public static RouteDataProvider Instance { get; private set; }

    public List<Route> Routes { get; private set; }
    public List<PointOfInterest> PointsOfInterest { get; private set; }

    #endregion

    #region Private Properties

    #endregion

    #region Framework Functions
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
    }

    private void OnEnable()
    {
        this.subscribeToEvents();

        this.StartCoroutine(downloadRoutes());
    }

    private void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events
    private void subscribeToEvents()
    {
        EventManager.OnRouteDataDownloaded += this.onRouteDataDownloadedEvent;
        EventManager.OnPointsOfInterestDownloaded += this.onPointsOfInterestDownloadedEvent;
    }
    private void unsubscribeFromEvents()
    {
        EventManager.OnRouteDataDownloaded -= this.onRouteDataDownloadedEvent;
        EventManager.OnPointsOfInterestDownloaded -= this.onPointsOfInterestDownloadedEvent;
    }

    #region Handle Events
    private void onPointsOfInterestDownloadedEvent(bool downloaded)
    {
        if (downloaded)
        {
            EventManager.RouteDataProviderReady(true);
        }
        else
        {
            EventManager.RouteDataProviderReady(false);
        }
    }
    private void onRouteDataDownloadedEvent(bool downloaded)
    {
        if (downloaded)
        {
            this.StartCoroutine(downloadPointsOfInterest());
        }
        else
        {
            EventManager.RouteDataProviderReady(false);
        }
    }
    #endregion

    #endregion

    #region Public Functions

    public void RestartDownload() 
    {
        this.StartCoroutine(downloadRoutes());
    }

    public Route GetRoute(string routeId)
    {
        Route retval = new Route();
        if (this.Routes != null)
        {
            foreach (Route route in this.Routes)
            {
                if (route.id == routeId)
                {
                    retval = route;
                    return retval;
                }
            }
        }
        return retval;
    }

    #endregion

    #region Private Functions

    private IEnumerator downloadRoutes()
    {
        bool success = false;
        string url = this.routeListURL;
        using (WWW www = new WWW(url))
        {
            while (!www.isDone)
            {
                yield return null;
            }
            if (www.error == null)
            {

                this.Routes = JsonConvert.DeserializeObject<List<Route>>(www.text, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });
                success = true;
            }
            else
            {
                Debug.LogError("Failed downloading Setup Data JSON from: \n" + url, this);
            }
        }
        EventManager.RouteDataDownloaded(success);
    }

    private IEnumerator downloadPointsOfInterest()
    {
        bool success = false;
        string url = this.poiListURL;
        using (WWW www = new WWW(url))
        {
            while (!www.isDone)
            {
                yield return null;
            }
            if (www.error == null)
            {

                this.PointsOfInterest = JsonConvert.DeserializeObject<List<PointOfInterest>>(www.text, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });
                success = true;
            }
            else
            {
                Debug.LogError("Failed downloading Setup Data JSON from: \n" + url, this);
            }
        }
        EventManager.PointsOfInterestDownloaded(success);
    }

    #endregion

}
