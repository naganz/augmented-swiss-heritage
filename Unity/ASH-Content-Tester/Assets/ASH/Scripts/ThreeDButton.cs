using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Collider))]
public class ThreeDButton : MonoBehaviour
{

    #region Inspector Properties


    #endregion

    #region Public Properties

    public UnityEvent hitEvent;
    public UnityEvent noHitEvent;
    #endregion

    #region Private Properties

    private Camera camera;
    private GameObject button;
    #endregion

    #region Framework Functions

    private void Start()
    {
        button = this.gameObject;
        camera = GameObject.FindGameObjectWithTag("MapCamera").GetComponent<Camera>() as Camera;
    }

    private void Update()
    {
        if (Application.isEditor)
        {
            this.checkMouseClick();
        }
        else
        {
            this.checkTouchClick();
        }
    }

    #endregion

    #region Events

    #endregion

    #region Public Functions

    #endregion

    #region Private Functions
    private void checkMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject == button && !EventSystem.current.IsPointerOverGameObject())
                {
                    handleHit();
                    //Debug.Log("Clicked");
                }
            }
            else
            {
                handleNoHit();
                //Debug.Log("Not Clicked");
            }

        }
    }

private void checkTouchClick()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = camera.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject == button && !EventSystem.current.IsPointerOverGameObject())
                {
                    handleHit();
                    //Debug.Log("Touched");

                }
            }
            else
            {
                handleNoHit();
                //Debug.Log("Not Touched");

            }
        }
    }

    private void handleHit()
    {
        hitEvent.Invoke();
    }

    private void handleNoHit()
    {
        noHitEvent.Invoke();
    }
    #endregion

}
