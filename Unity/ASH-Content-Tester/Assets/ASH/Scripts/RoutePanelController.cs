using ASH.Structures;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Mapbox.Unity.Location;
using Mapbox.Utils;
using System.Collections.Generic;
using System.Linq;

public class RoutePanelController : MonoBehaviour
{

    #region Inspector Properties

    [SerializeField]
    Text title;

    [SerializeField]
    Text subline;

    //[SerializeField]
    //TextMeshProUGUI distance;

    //[SerializeField]
    //private int maxDistance;

    [SerializeField]
    Image image;

    [Header("Config Values")]
    [SerializeField]
    private EHamburgerMenuState menuState;

    #endregion

    #region Public Properties

    public Route Route { get; private set; }

    #endregion

    #region Private Properties

    private Sprite sprite;
    private AsyncOperationHandle<Sprite> handle;

    ILocationProvider _locationProvider;
    ILocationProvider LocationProvider
    {
        get
        {
            if (_locationProvider == null && LocationProviderFactory.Instance != null)
            {
                _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
            }

            return _locationProvider;
        }
    }

    #endregion

    #region Framework Functions

    //void Start()
    //{
    //    this.subscribeToEvents();
    //}

    //void OnDisable()
    //{
    //    this.unsubscribeFromEvents();
    //}

    private void OnDestroy()
    {
        if (sprite != null)
        {
            Addressables.Release(this.handle);
        }
    }

    #endregion

    #region Events
    //private void subscribeToEvents()
    //{
    //    LocationProvider.OnLocationUpdated += onLocationUpdated;
    //}

    //private void unsubscribeFromEvents()
    //{
    //    LocationProvider.OnLocationUpdated -= onLocationUpdated;
    //}

    //private void onLocationUpdated(Location obj)
    //{
    //    this.updateDistance();
    //}

    #endregion

    #region Public Functions

    public void init(Route route)
    {
        this.Route = route;
        this.title.text = route.Name;
        int poiCount = RouteDataProvider.Instance.PointsOfInterest.Count(p => p.RouteId == route.id);
        string poiWord = poiCount == 1 ? "Sehenswürdigkeit" : "Sehenswürdigkeiten";
        this.subline.text = poiCount + " " + poiWord;
        //this.subline.text = $"{RouteDataProvider.Instance.PointsOfInterest.Count(p => p.RouteId == route.id)} Sehenswürdigkeiten";

        string addressableName = route.TenantId + "_" + route.id + "_Foto";
        Addressables.InitializeAsync().Completed += (handle) =>
        {
            Addressables.LoadAssetAsync<Sprite>(addressableName).Completed += loadingPanelDone;
        };
    }

    public void HandleDetailButtonClick()
    {
        EventManager.HamburgerMenuActiveState = this.menuState;
    }
    #endregion

    #region Private Functions

    //private void updateDistance()
    //{
    //    if (LocationProvider != null && RouteDataProvider.Instance.PointsOfInterest != null)
    //    {
    //        Vector2d currentLocation = LocationProvider.CurrentLocation.LatitudeLongitude;
    //        List<PointOfInterest> pois = RouteDataProvider.Instance.PointsOfInterest;
    //        double dist = getMinDistance(currentLocation, pois);
    //        //Debug.Log("RoutePanelController " + dist);
    //        distance.text = Helpers.FormatDistance(dist, maxDistance);
    //    }
    //}

    private double getMinDistance(Vector2d myLocation, List<PointOfInterest> pois)
    {
        double dist = double.MaxValue;
        Debug.Log("Route " + Route.id);
        foreach (PointOfInterest poi in pois)
        {
            if (poi.RouteId == Route.id && Helpers.IsActivePoi(poi, false))
            {
                Vector2d poiLocation = new Vector2d(poi.location.coordinates[1], poi.location.coordinates[0]);
                dist = Mathd.Min(dist, Helpers.GetGeoDistance(myLocation, poiLocation));
            }
        }
        return dist;
    }

    private void loadingPanelDone(AsyncOperationHandle<Sprite> handle)
    {
        this.handle = handle;

        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            Sprite _image = handle.Result;
            if (_image != null)
            {
                this.image.sprite = _image;
            }
        }
        else if (handle.Status == AsyncOperationStatus.Failed)
        {
            Debug.Log(handle.DebugName + " sprite could not be loaded.");
        }
    }

    #endregion

}
