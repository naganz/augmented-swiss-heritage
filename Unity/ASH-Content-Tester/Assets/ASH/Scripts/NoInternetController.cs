using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class NoInternetController : MonoBehaviour
{
    #region Inspector Properties

    [SerializeField]
    private GameObject noInternet;

    [SerializeField]
    private GameObject reloadButton;

    [SerializeField]
    private float delay;
    #endregion

    #region Framework Functions

    private void OnEnable()
    {
        this.subscribeToEvents();
    }

    private void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events
    private void subscribeToEvents()
    {
        EventManager.OnRouteDataProviderReady += onRouteDataProviderReady;
    }


    private void unsubscribeFromEvents()
    {
        EventManager.OnRouteDataProviderReady -= onRouteDataProviderReady;
      
    }

    #region Handle Events
    private async void onRouteDataProviderReady(bool downloaded)
    {
        this.noInternet.SetActive(!downloaded);
        this.reloadButton.SetActive(false);

        if (!downloaded) 
        {
            await Task.Delay(TimeSpan.FromSeconds(this.delay));
            this.reloadButton.SetActive(true);
        }
    }
    #endregion

    #region Public Functions
    public void HandleClick()
    {
        RouteDataProvider.Instance.RestartDownload();
    }
    #endregion

    #endregion
}
