using ASH.Structures;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Registrierkarte_RoutenController : MonoBehaviour
{

    #region Inspector Properties

    [Header("Scene Objects")]
    [SerializeField]
    private Text routenName;

    [SerializeField]
    Text subline;

    [SerializeField]
    private Text description;

    [SerializeField]
    Image image;

    [SerializeField]
    Transform allStationsContainer;

    [SerializeField]
    GameObject stationPanelPrefab;

    private AsyncOperationHandle<Sprite> handle;
    //    [SerializeField]
    //    private Image image;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties


    #endregion

    #region Framework Functions

    void OnEnable()
    {

        this.subscribeToEvents();

    }

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events

    private void subscribeToEvents()
    {
        EventManager.OnCurrentlyActiveRouteChanged += onCurrentlyActiveRouteChanged;
    }

    private void unsubscribeFromEvents()
    {
        EventManager.OnCurrentlyActiveRouteChanged -= onCurrentlyActiveRouteChanged;
    }
    private void onCurrentlyActiveRouteChanged(Route route)
    {
        this.updatePanel();
        spawnStations();
    }

    #endregion

    #region Public Functions

    #endregion

    #region Private Functions
    private void loadingPanelDone(AsyncOperationHandle<Sprite> handle)
    {
        this.handle = handle;

        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            Sprite _image = handle.Result;
            if (_image != null)
            {
                this.image.sprite = _image;
            }
        }
        else if (handle.Status == AsyncOperationStatus.Failed)
        {
            Debug.Log(handle.DebugName + " sprite could not be loaded.");
        }
    }
    private void updatePanel()
    {
        Route route = EventManager.CurrentlyActiveRoute;

        if (route != null)
        {
            this.routenName.text = route.Name;
            this.description.text = route.Description;
            int poiCount = RouteDataProvider.Instance.PointsOfInterest.Count(p => p.RouteId == route.id);
            string poiWord = poiCount == 1 ? "Sehenswürdigkeit" : "Sehenswürdigkeiten";
            this.subline.text = poiCount + " " + poiWord;
            //this.subline.text = $"{RouteDataProvider.Instance.PointsOfInterest.Count(p => p.RouteId == route.id)} Sehenswürdigkeiten";
            //           this.image.sprite = route.Sprite;
            string addressableName = route.TenantId + "_" + route.id + "_Foto";
            Addressables.InitializeAsync().Completed += (handle) =>
            {
                Addressables.LoadAssetAsync<Sprite>(addressableName).Completed += loadingPanelDone;
            };
        }
    }
    private void spawnStations()
    {
       Helpers.DestroyAllChildren(this.allStationsContainer.gameObject);
        foreach (PointOfInterest poi in RouteDataProvider.Instance.PointsOfInterest)
        {
            if (poi.RouteId == EventManager.CurrentlyActiveRoute.id)
            {
                GameObject instance = Instantiate(stationPanelPrefab);

                instance.transform.localPosition = Vector3.zero;
                instance.transform.localScale = Vector3.one;
                instance.transform.SetParent(this.allStationsContainer);

                StationPanelController stationPanelController = instance.GetComponent<StationPanelController>();
                RectTransform rectTrans = instance.GetComponent<RectTransform>();
                rectTrans.localScale = Vector3.one;
                rectTrans.localPosition = Vector3.zero;
                stationPanelController.Init(poi);
            }
        }

    }
    #endregion

}
