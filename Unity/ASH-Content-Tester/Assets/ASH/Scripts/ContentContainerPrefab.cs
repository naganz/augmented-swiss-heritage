using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class ContentContainerPrefab : MonoBehaviour
{
    [SerializeField]
    GameObject rotateHint;

    ObjectManipulator objManipulator;
    Interactable interactable;
    GameObject station;
    bool hasBounds = false;
    Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
    BoxCollider colliderPrefab;
    float zScaling = 1.0f;

    AudioSource audioSource;
    bool audioShouldPlay = true;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    // Start is called before the first frame update
    private void Start()
    {
        objManipulator = GetComponent<ObjectManipulator>();
        interactable = GetComponent<Interactable>();
        station = gameObject.transform.GetChild(transform.childCount - 1).gameObject;
        zScaling = station.transform.localScale.z;
        Debug.Log(zScaling);
        colliderPrefab = (BoxCollider)gameObject.GetComponent<Collider>();
        audioSource = station.GetComponent<AudioSource>();
        GetExtentOfPrefab();
    }

    public void Select()
    {
        EventManager.SendSelectContent(this.gameObject);
    }

    void Toggle3DEdit()
    {
        objManipulator.enabled = !objManipulator.enabled;
        interactable.enabled = !interactable.enabled;
        rotateHint.SetActive(!rotateHint.activeSelf);
        colliderPrefab.enabled = !colliderPrefab.enabled;

        if (audioSource.isPlaying)
        {
            audioSource.Pause();
        }
        else
        {
            if (audioShouldPlay)
            {
                audioSource.Play();
            }
        }
    }

    void SetAudioShouldPlay(bool val)
    {
        audioShouldPlay = val;
    }


    void GetExtentOfPrefab()
    {
        GetBounds(station);

        Vector3 size = bounds.size;
        if (zScaling < 1) zScaling = 1.0f;
        size = new Vector3(size.x, size.y, size.z / zScaling);

        colliderPrefab.center = bounds.center - gameObject.transform.position;
        colliderPrefab.size = size;
    }

    void GetBounds(GameObject obj)
    {
        for (int i = 0; i < obj.transform.childCount; ++i)
        {
            GameObject child = obj.transform.GetChild(i).gameObject;
            Renderer childRenderer = child.GetComponent<Renderer>();
            if (childRenderer != null)
            {
                if (hasBounds)
                {
                    bounds.Encapsulate(childRenderer.bounds);
                }
                else
                {
                    bounds = childRenderer.bounds;
                    hasBounds = true;
                }
            }

            if (child.transform.childCount > 0)
            {
                GetBounds(child);
            }
        }
    }


    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR += Toggle3DEdit;
        EventManager.OnSetAudioVarFor3DPlace += SetAudioShouldPlay;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR -= Toggle3DEdit;
        EventManager.OnSetAudioVarFor3DPlace -= SetAudioShouldPlay;
    }
    #endregion
}
