using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleXRPlaceReseter : MonoBehaviour
{
    Toggle toggle;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    private void Start()
    {
        toggle = GetComponent<Toggle>();
    }

    void ResetButton()
    {
        toggle.isOn = true;
    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnCloseARAction += ResetButton;
        EventManager.OnRefreshAR += ResetButton;
    }

    void UnRegisterEvents()
    {
        EventManager.OnCloseARAction -= ResetButton;
        EventManager.OnRefreshAR -= ResetButton;
    }
    #endregion
}
