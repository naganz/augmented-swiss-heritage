using ASH.Structures;
using MagneticScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class StationPanelSpawner : MonoBehaviour
{

    #region Inspector Properties

    [SerializeField]
    GameObject stationPanelPrefab;

    [SerializeField]
    Transform allStationsContainer;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Framework Functions

    void OnEnable()
    {
        this.subscribeToEvents();
    }

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events
    private void subscribeToEvents()
    {
        EventManager.OnRouteDataProviderReady += this.onRouteDataProviderReady;
    }

    private void unsubscribeFromEvents()
    {
        EventManager.OnRouteDataProviderReady -= this.onRouteDataProviderReady;
    }

    private void onRouteDataProviderReady(bool ready)
    {
        if (ready)
        {
            spawnStations();
        }
    }

    #endregion

    #region Public Functions

    #endregion

    #region Private Functions

    private void spawnStations()
    {
        Helpers.DestroyAllChildren(this.allStationsContainer.gameObject);
        foreach (PointOfInterest poi in RouteDataProvider.Instance.PointsOfInterest)
        {
            GameObject instance = Instantiate(stationPanelPrefab);

            instance.transform.localPosition = Vector3.zero;
            instance.transform.localScale = Vector3.one;
            instance.transform.SetParent(this.allStationsContainer);

            StationPanelController stationPanelController = instance.GetComponent<StationPanelController>();
            RectTransform rectTrans = instance.GetComponent<RectTransform>();
            rectTrans.localScale = Vector3.one;
            rectTrans.localPosition = Vector3.zero;
            stationPanelController.Init(poi);
        }

    }

    #endregion
}
