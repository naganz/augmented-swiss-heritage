using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveHamburgerMenuState : MonoBehaviour
{

    #region Inspector Properties

    [Header("Config Values")]
    [SerializeField]
    private List<EHamburgerMenuState> menuStates;

    [Header("Scene Objects")]
    [SerializeField]
    private GameObject content;
    #endregion

    #region Framework Functions

    void OnEnable()
    {

        this.subscribeToEvents();

    }

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events

    private void subscribeToEvents()
    {
        EventManager.OnHamburgerMenuActiveStateChanged += onHamburgerMenuActiveStateChanged;
    }

    private void unsubscribeFromEvents()
    {

        EventManager.OnHamburgerMenuActiveStateChanged -= onHamburgerMenuActiveStateChanged;

    }

    private void onHamburgerMenuActiveStateChanged(EHamburgerMenuState e)
    {

        this.content.SetActive(menuStates.Contains(EventManager.HamburgerMenuActiveState));
    }

    #endregion

}
