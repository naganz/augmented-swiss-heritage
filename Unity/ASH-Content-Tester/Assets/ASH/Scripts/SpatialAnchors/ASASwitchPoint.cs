using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ASASwitchPoint : MonoBehaviour
{
    [SerializeField]
    [Tooltip("ASA GameObject to activate on iOS & Android")]
    private GameObject spatialAnchorsScripts;

    // Start is called before the first frame update
    void Start()
    {
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            spatialAnchorsScripts.SetActive(true);
            Debug.Log("ASA: SpatialanchorsScripts activated (iOS or Android)");
        } else
        {
            Debug.LogWarning("ASA: SpatialanchorsScripts not activated because it's only running on iOS or Android");

        }

    }


}
