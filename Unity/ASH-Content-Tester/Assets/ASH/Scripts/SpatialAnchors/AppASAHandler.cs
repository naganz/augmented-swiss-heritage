 using Microsoft.Azure.SpatialAnchors.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.Azure.SpatialAnchors;
using UnityEngine.UI;
using System.Threading.Tasks;
using System;
using UnityEngine.Networking;
using Newtonsoft.Json;
using AnchorContent.Structures;
using System.Linq;
using Microsoft.ApplicationInsights.DataContracts;

public class AppASAHandler : MonoBehaviour
{
    #region Unity Inspector Variables
    [SerializeField]
    [Tooltip("SpatialAnchorManager instance. This is required.")]
    private SpatialAnchorManager cloudManager = null;

    
    [SerializeField]
    [Tooltip("Prefab for Anchor.")]
    private GameObject prefab;

   
    
    [SerializeField]
    [Tooltip("Content Service: URL for 3D Content")]
    private string contentServiceContentUrl ;

    [SerializeField]
    [Tooltip("ID of the Project")]
    private string projectId;

    
    #endregion // Unity Inspector Variables

    #region Local Properties
    protected readonly List<string> anchorIdsToLocate = new List<string>();
    protected AnchorLocateCriteria anchorLocateCriteria ;
    private PlatformLocationProvider sensorProvider;
    private List<ContentPlaced> anchorContentList;
    private string currentPrefabName;
  //  private GameObject anchorGameObject;
    #endregion

    private void Start()
    {
        #region PlatformLocationProvider
        sensorProvider = new PlatformLocationProvider();
        Debug.Log("ASA Sensor Provider created");
        #endregion

        //anchorLocateCriteria = new AnchorLocateCriteria();
    }

    #region Enable / Disable Events
    private void OnEnable()
    {
        anchorLocateCriteria = new AnchorLocateCriteria();
        
         
        if (cloudManager == null) Debug.LogWarning("ASA: cloudManager is null in OnEnable()");
        cloudManager.Error += CloudManager_Error;
        cloudManager.SessionStarted += CloudManager_SessionStarted;
        cloudManager.AnchorLocated += CloudManager_AnchorLocated;
        EventManager.OnStartAsaSession += EventManager_OnStartAsaSession;
        EventManager.OnRestart += EventManager_OnRestart;
        EventManager.OnCloseARAction += EventManager_OnCloseARAction;
    //    if (Input.location.isEnabledByUser) Input.location.Start();

    }

    private void OnDisable()
    {
        cloudManager.Error -= CloudManager_Error;
        cloudManager.SessionStarted -= CloudManager_SessionStarted;
        cloudManager.AnchorLocated -= CloudManager_AnchorLocated;
        EventManager.OnStartAsaSession -= EventManager_OnStartAsaSession;
        EventManager.OnRestart -= EventManager_OnRestart;
        EventManager.OnCloseARAction -= EventManager_OnCloseARAction;

        //    if (Input.location.status == LocationServiceStatus.Running) Input.location.Stop();
    }
    #endregion

    #region App Events
    
    private void EventManager_OnCloseARAction()
    {
        Debug.Log("ASA: StopASASession (start stopping)");
        // cloudManager.StopSession();
        cloudManager.StopSession();
    }
    private async void EventManager_OnStartAsaSession(ASH.Structures.PointOfInterest pointOfInterest)
    {
        this.currentPrefabName = pointOfInterest.PrefabName;

        await CreateAndStartSession();
        CreateWatcher();
    }
    private void EventManager_OnRestart()
    {
        Debug.Log("ASA Restart");
        cloudManager.ResetSessionAsync();
        //if (this.anchorGameObject != null)
        //{
        //    Destroy(anchorGameObject);
        //    this.anchorGameObject = null;
            
        //}
    }
    
    #endregion

    #region Create Session, LoadContentData & Watcher
    public async Task CreateAndStartSession()
    {
        if (cloudManager.Session == null)
        {
            await cloudManager.CreateSessionAsync();
            Debug.Log($"ASA Session created");
        }

        Debug.Log($"ASA Session configured");
        if (anchorContentList == null || anchorContentList.Count == 0) await Load3DContent();
        var keys = this.anchorContentList.Where(cp => cp.Prefab == this.currentPrefabName).Select(cp => cp.AnchorId).Distinct().ToArray();
        anchorLocateCriteria.Identifiers = keys;
        Debug.Log($"ASA Configured Anchor Identifiers: {string.Join(",",keys)}");

        await cloudManager.StartSessionAsync();
        Debug.Log($"ASA Session started: {cloudManager.IsSessionStarted}");
    }
    protected CloudSpatialAnchorWatcher CreateWatcher()
    {
        if ((cloudManager != null) && (cloudManager.Session != null))
        {
            Debug.Log($"ASA Create Watcher");
            return cloudManager.Session.CreateWatcher(anchorLocateCriteria);
        }
        else
        {
            return null;
        }
    }

    private async Task Load3DContent()
    {
        var url = this.contentServiceContentUrl.Replace("{projectid}", this.projectId);
        Debug.Log($"ASA Content: url = {url}");
        using (var request = UnityWebRequest.Get(url))
        {

            request.SendWebRequest();
            while (!request.isDone)
                await Task.Delay(100);
            if (request.result == UnityWebRequest.Result.Success)
            {
                var json = request.downloadHandler.text;
                Debug.Log($"ASA Content: {json}");
                this.anchorContentList = JsonConvert.DeserializeObject<List<ContentPlaced>>(json, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });
            }
            else
            {
                string em = $"ASA Content: Error on Loading: {request.error}";
                Debug.LogError(em);
                AppInsights.Instance.TrackTrace(em, SeverityLevel.Error);
            }
        }
    }
    #endregion

    #region ASA Events
    private void CloudManager_Error(object sender, SessionErrorEventArgs args)
    {
        EventManager.SendShowErrorr($"Error SpatialAnchorManager:  {args.ErrorMessage}");
        Debug.LogError($"ASA Error: {args.ErrorMessage}");
    }
    private void CloudManager_SessionStarted(object sender, EventArgs e)
    {
        cloudManager.Session.LocationProvider = sensorProvider;
        cloudManager.Session.LocationProvider.Start();
        Debug.Log($"ASA Session Started.");
 

    }
    private void CloudManager_AnchorLocated(object sender, AnchorLocatedEventArgs args)
    {
        try
        {
            Debug.Log($"ASA Anchor Found: {args?.Anchor?.Identifier}");
            if (args != null && args.Anchor != null && args.Status == LocateAnchorStatus.Located)
            {
                UnityDispatcher.InvokeOnAppThread(() =>
                {
                    Pose anchorPose = Pose.identity;

#if UNITY_ANDROID || UNITY_IOS
                    anchorPose = args.Anchor.GetPose();
#endif
                    Debug.Log($"ASA Anchor Located: {args.Anchor.Identifier}, position: {anchorPose.position}");
                    var oldAnchor = GameObject.Find(args.Anchor.Identifier);
                    if (oldAnchor != null)
                    {
                        Destroy(oldAnchor);
                    }

                    if (prefab == null) prefab = new GameObject();
                    var root = GameObject. Instantiate(prefab, anchorPose.position, anchorPose.rotation);
                    root.name = args.Anchor.Identifier;

                    foreach (var place in this.anchorContentList.Where(ac => ac.AnchorId == args.Anchor.Identifier))
                    {
                        EventManager.SendSpawnModel(root, place);
                    }
                    Debug.Log($"ASA: Anchor '{args.Anchor.Identifier}' created");

                });
            }
        }
        catch (Exception ex)
        {
            Debug.LogError($"ASA: Error: {ex.Message}");
            AppInsights.Instance.TrackException(ex);
        }
    }

    #endregion


    
}
