using ASH.Structures;
using Mapbox.Unity.Location;
using Mapbox.Utils;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class Registrierkarte_StationController : MonoBehaviour
{

    #region Inspector Properties

    [Header("Scene Objects")]
    [SerializeField]
    private Text titel;

    [SerializeField]
    private Text category;

    [SerializeField]
    private Text distance;

    [SerializeField]
    private int maxDistance;

    [SerializeField]
    private Text description;

    [SerializeField]
    private Image image;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    private AsyncOperationHandle<Sprite> handle;

    ILocationProvider _locationProvider;
    ILocationProvider locationProvider
    {
        get
        {
            if (_locationProvider == null && LocationProviderFactory.Instance != null)
            {
                _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
            }

            return _locationProvider;
        }
    }

    private bool onLocationUpdatedEventAdded = false;

    #endregion

    #region Framework Functions

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    void Update()
    {
        if (!onLocationUpdatedEventAdded && locationProvider != null)
        {
            Debug.Log("Registrierkarte subscribeToEvents");
            this.subscribeToEvents();
            onLocationUpdatedEventAdded = true;
        }
    }

    #endregion

    #region Events

    private void subscribeToEvents()
    {
        EventManager.OnCurrentlyActivePoiChanged += onCurrentlyActivePoiChanged;
        locationProvider.OnLocationUpdated += onLocationUpdated;
    }

    private void unsubscribeFromEvents()
    {
        EventManager.OnCurrentlyActivePoiChanged -= onCurrentlyActivePoiChanged;
        locationProvider.OnLocationUpdated -= onLocationUpdated;
        onLocationUpdatedEventAdded = false;
    }

    private void onCurrentlyActivePoiChanged(PointOfInterest poi)
    {
        this.updatePanel();
    }

    private void onLocationUpdated(Location obj)
    {
        if (locationProvider != null && RouteDataProvider.Instance.PointsOfInterest != null)
        {
            Debug.Log("Registrierkarte onLocationUpdated");
            this.updateLocationOnPanel();
        }
    }

    #endregion

    #region Public Functions

    public void HandleNavClick()
    {
        AppPointOfInterest poi = EventManager.CurrentlyActivePoi;
        string address = poi.location.coordinates[1].ToString().Replace(",",".") + "," + poi.location.coordinates[0].ToString().Replace(",", ".");
        Debug.Log("Go to: https://www.google.com/maps/dir/?api=1&origin=My+Location&destination=" + address + "&travelmode=walking");

#if UNITY_ANDROID
        Application.OpenURL("google.navigation:q="+ address +"&mode=w");
#elif UNITY_IOS
            Application.OpenURL("https://www.google.com/maps/dir/?api=1&origin=My+Location&destination=" + address + "&travelmode=walking");
#else
        Application.OpenURL("https://www.google.com/maps/dir/?api=1&origin=My+Location&destination=" + address + "&travelmode=walking");
#endif
    }

    public void HandleARClick()
    {
        //Start AR Session with POI info
        // string addressableName = poi.TenantId + "_" + poi.id + "_AR";
        EventManager.SendOpenARAction(EventManager.CurrentlyActivePoi);
    }
    #endregion

    #region Private Functions

    private void updatePanel()
    {
        AppPointOfInterest poi = EventManager.CurrentlyActivePoi;

        if (poi != null)
        {
            this.titel.text = poi.Name;

            string cateogry = poi.PoiTypes[0].ToString();
            this.category.text = cateogry.Replace("_", " ");

            this.updateLocationOnPanel();

            this.description.text = poi.Description;
            //this.image.sprite = poi.Sprite;
            string addressableName = poi.TenantId + "_" + poi.id + "_Foto";
            Addressables.InitializeAsync().Completed += (handle) =>
            {
                Addressables.LoadAssetAsync<Sprite>(addressableName).Completed += loadingPanelDone;
            };
        }
    }



    private void updateLocationOnPanel()
    {
        AppPointOfInterest poi = EventManager.CurrentlyActivePoi;

        if (poi != null && locationProvider != null)
        {
            double dist = this.getDistance(poi);
            distance.text = Helpers.FormatDistance(dist, maxDistance);
        }
    }


    private double getDistance(PointOfInterest poi)
    {
        double distance = -1;

        Vector2d a = locationProvider.CurrentLocation.LatitudeLongitude;
        Vector2d b = new Vector2d(poi.location.coordinates[1], poi.location.coordinates[0]);

        distance = Helpers.GetGeoDistance(a, b);

        return distance;
    }

    private void loadingPanelDone(AsyncOperationHandle<Sprite> handle)
    {
        this.handle = handle;

        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            Sprite _image = handle.Result;
            if (_image != null)
            {
                this.image.sprite = _image;
            }
        }
        else if (handle.Status == AsyncOperationStatus.Failed)
        {
            Debug.Log(handle.DebugName + " sprite could not be loaded.");
        }
    }
    #endregion

}
