using Mapbox.Unity.Map;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ASH.Structures;
using Mapbox.Utils;

public class PinSpawner : MonoBehaviour
{

    #region Inspector Properties
    [SerializeField]
    AbstractMap map;

    [SerializeField]
    GameObject pinPrefab;

    [SerializeField]
    Transform pinContainer;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Framework Functions

    void OnEnable()
    {
        this.subscribeToEvents();
    }

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events

    private void subscribeToEvents()
    {
        EventManager.OnRouteDataProviderReady += this.onRouteDataProviderReady;
    }

    private void unsubscribeFromEvents()
    {
        EventManager.OnRouteDataProviderReady -= this.onRouteDataProviderReady;
    }

    private void onRouteDataProviderReady(bool ready)
    {
        if (ready)
        {
            spawnPOIs();
        }
    }

    #endregion

    #region Public Functions

    #endregion

    #region Private Functions
    private void spawnPOIs()
    {
        Helpers.DestroyAllChildren(this.pinContainer.gameObject);
        foreach (PointOfInterest poi in RouteDataProvider.Instance.PointsOfInterest)
        {
            GameObject instance = Instantiate(pinPrefab);

            instance.transform.SetParent(this.pinContainer);

            PinController pinController = instance.GetComponent<PinController>();
            pinController.Init(poi, map);
        }
    }

    #endregion

}
