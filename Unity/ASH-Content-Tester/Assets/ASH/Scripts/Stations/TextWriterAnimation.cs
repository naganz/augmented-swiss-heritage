using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextWriterAnimation : MonoBehaviour
{
    [SerializeField]
    string text;

    private string displayedText = "";
    TMP_Text textObj;

    void Start()
    {
        textObj = GetComponent<TMP_Text>();
    }

    public void ShowTextWriterAnimation()
    {
        StartCoroutine(AnimateText());
    }

    IEnumerator AnimateText()
    {
        for (int i = 0; i < text.Length; i++)
        {
            displayedText = text.Substring(0, i);
            textObj.text = displayedText;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
