using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaveStation8 : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0)
            return;

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

        if(Physics.Raycast(ray, out hit))
        {
            if (hit.transform.tag == "Painting")
            {
                transform.position = hit.point;
            }
        }
    }
}
