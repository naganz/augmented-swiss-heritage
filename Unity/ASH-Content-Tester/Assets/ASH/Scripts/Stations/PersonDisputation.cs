using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PersonDisputation : MonoBehaviour
{
    [SerializeField]
    bool isActive;

    [SerializeField]
    TMP_Text textBubble;

    [SerializeField]
    List<string> keywords = new List<string>();

    [SerializeField]
    PersonDisputation otherPerson;

    BoxCollider objCollider;
    Material material;

    int sineGlowPropertyId;
    int currentKeywordNr = 0;


    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    void Start()
    {
        objCollider = GetComponent<BoxCollider>();
        material = GetComponent<SpriteRenderer>().material;

        sineGlowPropertyId = Shader.PropertyToID("_SineGlowFade");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0 || !isActive)
            return;

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == transform)
                {
                    // Disable highlighting and change text
                    material.SetFloat(sineGlowPropertyId, 0.0f);

                    if (currentKeywordNr < keywords.Count)
                    {
                        textBubble.text = keywords[currentKeywordNr];
                        currentKeywordNr++;
                        otherPerson.SetIsActive(true);
                    }

                    isActive = false;
                }
            }
        }
    }

    public void SetIsActive(bool val)
    {
        isActive = val;

        if (val && currentKeywordNr < keywords.Count)
        {
            material.SetFloat(sineGlowPropertyId, 1.0f);
        }
    }

    private void EnableBoxCollider()
    {
        if (objCollider != null)
        {
            objCollider.enabled = !objCollider.enabled;
        }
    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR += EnableBoxCollider;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR -= EnableBoxCollider;
    }
    #endregion
}
