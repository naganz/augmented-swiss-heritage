using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateDisputationDialogue : MonoBehaviour
{
    [SerializeField]
    List<BoxCollider> objsToActivate = new List<BoxCollider>();

    [SerializeField]
    PersonDisputation firstPerson;

    AudioSource audioSource;
    double clipLength;
    bool interactionIsEnabled = false;

    private MuteSwitchDetector detector;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        clipLength = audioSource.clip.length;

        if (audioSource != null)
        {
            audioSource.PlayDelayed(3.0f);
        }

#if UNITY_IOS && !UNITY_EDITOR
        detector = transform.parent.GetChild(1).GetComponent<MuteSwitchDetector>();
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if (interactionIsEnabled) return;

        if (!audioSource.isPlaying)
        {
            if (audioSource.time == clipLength)
            {
                // Enable interaction
                foreach (BoxCollider col in objsToActivate)
                {
                    col.enabled = true;
                }

                firstPerson.SetIsActive(true);
                interactionIsEnabled = true;
                EventManager.SendSetAudioVarFor3DPlace(false);
            }
        }
        else
        {
            // Check if sound volume is muted
            if (AudioSettings.Mobile.muteState)
            {
                EventManager.SendDisplayWarningSound(true);
            }
            else
            {
                EventManager.SendDisplayWarningSound(false);

#if UNITY_IOS && !UNITY_EDITOR
                if (detector != null)
                {
                    if (detector.isMuted) {
                        EventManager.SendDisplayWarningSound(true);
                    }
                }
#endif
            }
        }
    }
}
