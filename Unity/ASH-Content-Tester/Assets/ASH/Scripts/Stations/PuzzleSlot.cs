using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class PuzzleSlot : MonoBehaviour
{
    [SerializeField]
    GameObject puzzleSlot;

    BoxCollider puzzleCol;
    ObjectManipulator puzzleObjMan;

    float distance = 0.1f;
    bool isPlaced = false;

    PlayAudioOfInteractiveStation playAudio;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    private void Start()
    {
        puzzleCol = GetComponent<BoxCollider>();
        puzzleObjMan = GetComponent<ObjectManipulator>();
        playAudio = GetComponentInParent<PlayAudioOfInteractiveStation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlaced) return;

        if (Vector2.Distance(transform.position, puzzleSlot.transform.position) < distance)
        {
            // Deactivate movement of puzzle piece
            puzzleCol.enabled = false;
            puzzleObjMan.enabled = false;

            // Set position of puzzle piece to slot.
            transform.position = puzzleSlot.transform.position;

            isPlaced = true;
            playAudio.IncrementFinishedTasks();
        }
    }

    void ActivateObjManipulator()
    {
        if (isPlaced) return;

        // Deactivate object manipulator while user manipulates whole station gameobject.
        if (puzzleObjMan != null)
        {
            puzzleObjMan.enabled = !puzzleObjMan.enabled;
        }
        
        if (puzzleCol != null)
        {
            puzzleCol.enabled = !puzzleCol.enabled;
        }
    }


    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR += ActivateObjManipulator;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR -= ActivateObjManipulator;
    }
    #endregion
}
