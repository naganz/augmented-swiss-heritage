using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickBubble : MonoBehaviour
{
    [SerializeField]
    TextWriterAnimation textWriterAnimation;

    [SerializeField]
    GameObject bubble;

    BoxCollider objCollider;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    void Start()
    {
        objCollider = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == transform)
                {
                    // Show bubble and start text writer animation
                    bubble.SetActive(true);
                    textWriterAnimation.ShowTextWriterAnimation();
                }
            }
        }
    }

    private void EnableBoxCollider()
    {
        if (objCollider != null)
        {
            objCollider.enabled = !objCollider.enabled;
        }
    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR += EnableBoxCollider;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR -= EnableBoxCollider;
    }
    #endregion
}
