using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintingFilled : MonoBehaviour
{
    [SerializeField]
    AudioManagerSwiping audioManagerSwiping;

    bool isVisible = false;              // Becomes true if painting is filled and visible (threshold reached)
    int thresholdPaintingFilled = 90;       // Threshold to define when painting is filled (in percent)

    string currentPercentageOfPainting = "0";

    public string percentage
    {
        get
        {
            return currentPercentageOfPainting;
        }
        set
        {
            if (currentPercentageOfPainting != value)
            {
                currentPercentageOfPainting = value;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        Int32.TryParse(currentPercentageOfPainting, out int p);

        if (p >= thresholdPaintingFilled && !isVisible)
        {
            isVisible = true;
            audioManagerSwiping.AddFilledPainting();
        }
    }
}
