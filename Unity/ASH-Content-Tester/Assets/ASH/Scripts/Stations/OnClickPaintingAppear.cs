using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickPaintingAppear : MonoBehaviour
{
    PlayAudioOfInteractiveStation playAudio;

    Material material;
    int fadePropertyId;
    float fadeValue;
    int brightnessPropertyId;

    bool isTouched = false;
    BoxCollider objCollider;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    void Start()
    {
        material = GetComponent<SpriteRenderer>().material;

        fadePropertyId = Shader.PropertyToID("_FullDistortionFade");
        fadeValue = 0;

        brightnessPropertyId = Shader.PropertyToID("_Brightness");

        objCollider = GetComponent<BoxCollider>();

        playAudio = GetComponentInParent<PlayAudioOfInteractiveStation>();
    }

    
    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == transform)
                {
                    if (!isTouched)
                    {
                        playAudio.IncrementFinishedTasks();
                    }
                    
                    material.SetFloat(brightnessPropertyId, 1.0f);
                    isTouched = true;
                }
            }
        }

        // Fade alpha when user clicked on painting piece.
        if (isTouched && fadeValue < 1)
        {
            fadeValue += Time.deltaTime;
            if (fadeValue > 1) fadeValue = 1;
            material.SetFloat(fadePropertyId, fadeValue);
        }
    }

    private void EnableBoxCollider()
    {
        if (objCollider != null)
        {
            objCollider.enabled = !objCollider.enabled;
        }
    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR += EnableBoxCollider;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR -= EnableBoxCollider;
    }
    #endregion
}
