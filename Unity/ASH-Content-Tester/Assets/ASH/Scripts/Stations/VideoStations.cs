using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoStations : MonoBehaviour
{
    [SerializeField]
    Sprite playVideoSprite;
    [SerializeField]
    Sprite pauseVideoSprite;

    VideoPlayer videoPlayer;

    private MuteSwitchDetector detector;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        videoPlayer = GetComponent<VideoPlayer>();

#if UNITY_IOS && !UNITY_EDITOR
        detector = transform.parent.parent.GetChild(1).GetComponent<MuteSwitchDetector>();
#endif
    }

    void Update()
    {
        if (videoPlayer == null) return;

        // Change icon of audio button
        if (videoPlayer.isPlaying)
        {
            EventManager.SendChangeButtonIcon(pauseVideoSprite);

            // Check if sound volume is muted
            if (AudioSettings.Mobile.muteState)
            {
                EventManager.SendDisplayWarningSound(true);
            }
            else
            {
                EventManager.SendDisplayWarningSound(false);

#if UNITY_IOS && !UNITY_EDITOR
                if (detector != null)
                {
                    if (detector.isMuted) {
                        EventManager.SendDisplayWarningSound(true);
                    }
                }
#endif
            }
        }
        else
        {
            EventManager.SendChangeButtonIcon(playVideoSprite);
        }
    }

    void PlayPauseVideo()
    {
        if (videoPlayer != null)
        {
            if (!videoPlayer.isPlaying)
            {
                videoPlayer.Play();
            }
            else
            {
                videoPlayer.Pause();
            }
        }

    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggleVideoAudio += PlayPauseVideo;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggleVideoAudio -= PlayPauseVideo;
    }
    #endregion
}
