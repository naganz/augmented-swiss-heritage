using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickChangePainting : MonoBehaviour
{
    [SerializeField]
    private List<Sprite> sprites = new List<Sprite>();

    [SerializeField]
    private bool loopPics = false;

    private SpriteRenderer spriteRenderer;
    private int clickCounter = 0;

    BoxCollider objCollider;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        objCollider = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Painting")
                {
                    if (!loopPics && (clickCounter >= sprites.Count)) return;
                    spriteRenderer.sprite = sprites[clickCounter % sprites.Count];
                    clickCounter++;
                }
            }
        }
    }

    private void EnableBoxCollider()
    {
        if (objCollider != null)
        {
            objCollider.enabled = !objCollider.enabled;
        }
    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR += EnableBoxCollider;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR -= EnableBoxCollider;
    }
    #endregion
}
