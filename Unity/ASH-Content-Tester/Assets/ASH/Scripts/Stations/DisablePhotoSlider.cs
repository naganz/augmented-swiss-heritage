using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class DisablePhotoSlider : MonoBehaviour
{
    CanvasGroup canvasGroup;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    private void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void ToggleInteractionOfSlider()
    {
        canvasGroup.interactable = !canvasGroup.interactable;
        canvasGroup.blocksRaycasts = !canvasGroup.blocksRaycasts;
    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR += ToggleInteractionOfSlider;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR -= ToggleInteractionOfSlider;
    }
    #endregion
}
