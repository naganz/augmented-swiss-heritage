using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAnimationOnClick : MonoBehaviour
{
    [SerializeField]
    Animator anim;

    bool animPlayed = false;
    BoxCollider objCollider;
    PlayAudioOfInteractiveStation playAudio;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    private void Start()
    {
        objCollider = GetComponent<BoxCollider>();
        playAudio = GetComponentInParent<PlayAudioOfInteractiveStation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform == transform)
                {
                    if (!animPlayed)
                    {
                        anim.Play("Station20");
                        animPlayed = true;
                        playAudio.IncrementFinishedTasks();
                    }
                }
            }
        }
    }

    private void EnableBoxCollider()
    {
        if (objCollider != null)
        {
            objCollider.enabled = !objCollider.enabled;
        }
    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR += EnableBoxCollider;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR -= EnableBoxCollider;
    }
    #endregion
}
