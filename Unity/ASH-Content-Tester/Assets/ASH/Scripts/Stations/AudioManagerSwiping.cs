using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerSwiping : MonoBehaviour
{
    [SerializeField]
    int numberPaintings;

    int numberFilledPaintings = 0;
    AudioSource audioSource;
    double clipLength;
    bool audioHasFinished = false;

    private MuteSwitchDetector detector;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        EventManager.SendSetAudioVarFor3DPlace(false);

        clipLength = audioSource.clip.length;

#if UNITY_IOS && !UNITY_EDITOR
        detector = transform.parent.GetChild(1).GetComponent<MuteSwitchDetector>();
#endif
    }

    public void AddFilledPainting()
    {
        numberFilledPaintings++;

        if (numberPaintings == numberFilledPaintings)
        {
            // All paintings are visible, play audio
            audioSource.Play();
            EventManager.SendSetAudioVarFor3DPlace(true);
        }
    }

    private void Update()
    {
        if (audioHasFinished) return;

        if (!audioSource.isPlaying)
        {
            if (audioSource.time == clipLength)
            {
                EventManager.SendSetAudioVarFor3DPlace(false);
                audioHasFinished = true;
            }
        }
        else
        {
            // Check if sound volume is muted
            if (AudioSettings.Mobile.muteState)
            {
                EventManager.SendDisplayWarningSound(true);
            }
            else
            {
                EventManager.SendDisplayWarningSound(false);

#if UNITY_IOS && !UNITY_EDITOR
                if (detector != null)
                {
                    if (detector.isMuted) {
                        EventManager.SendDisplayWarningSound(true);
                    }
                }
#endif
            }
        }
    }
}
