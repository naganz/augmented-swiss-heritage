using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateInteraction : MonoBehaviour
{
    [SerializeField]
    List<GameObject> objsToActivate = new List<GameObject>();

    AudioSource audioSource;
    double clipLength;
    bool interactionIsEnabled = false;

    private MuteSwitchDetector detector;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        clipLength = audioSource.clip.length;

        if (audioSource != null)
        {
            audioSource.PlayDelayed(3.0f);
        }

#if UNITY_IOS && !UNITY_EDITOR
        detector = transform.parent.GetChild(1).GetComponent<MuteSwitchDetector>();
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if (interactionIsEnabled) return;

        if (!audioSource.isPlaying)
        {
            if (audioSource.time == clipLength)
            {
                // Enable interaction
                foreach (GameObject obj in objsToActivate)
                {
                    obj.SetActive(true);
                }

                interactionIsEnabled = true;
                EventManager.SendSetAudioVarFor3DPlace(false);
            }
        }
        else
        {
            // Check if sound volume is muted
            if (AudioSettings.Mobile.muteState)
            {
                EventManager.SendDisplayWarningSound(true);
            }
            else
            {
                EventManager.SendDisplayWarningSound(false);

#if UNITY_IOS && !UNITY_EDITOR
                if (detector != null)
                {
                    if (detector.isMuted) {
                        EventManager.SendDisplayWarningSound(true);
                    }
                }
#endif
            }
        }
    }
}
