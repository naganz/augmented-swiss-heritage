using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteAudio : MonoBehaviour
{
    [SerializeField]
    Sprite volumeOnSprite;

    [SerializeField]
    Sprite volumeOffSprite;

    AudioSource audioSource;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (audioSource == null) return;

        // Change icon of audio button
        if (audioSource.mute)
        {
            EventManager.SendChangeButtonIcon(volumeOffSprite);
        }
        else
        {
            EventManager.SendChangeButtonIcon(volumeOnSprite);
        }
    }

    private void ToggleMuteAudio()
    {
        audioSource.mute = !audioSource.mute;
    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggleVideoAudio += ToggleMuteAudio;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggleVideoAudio -= ToggleMuteAudio;
    }
    #endregion
}
