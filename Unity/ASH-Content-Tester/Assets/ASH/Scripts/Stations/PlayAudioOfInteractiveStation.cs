using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudioOfInteractiveStation : MonoBehaviour
{
    [SerializeField]
    private int neededTasks;
    [SerializeField]
    private float waitForSeconds = 0.0f;

    AudioSource audioSource;
    int finishedTasks;
    double clipLength;
    bool audioHasFinished = false;

    private MuteSwitchDetector detector;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        EventManager.SendSetAudioVarFor3DPlace(false);
        clipLength = audioSource.clip.length;

#if UNITY_IOS && !UNITY_EDITOR
        detector = transform.parent.GetChild(1).GetComponent<MuteSwitchDetector>();
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if (audioHasFinished) return;

        if (!audioSource.isPlaying)
        {
            if (audioSource.time == clipLength)
            {
                EventManager.SendSetAudioVarFor3DPlace(false);
                audioHasFinished = true;
            }
        }
        else
        {
            // Check if sound volume is muted
            if (AudioSettings.Mobile.muteState)
            {
                EventManager.SendDisplayWarningSound(true);
            }
            else
            {
                EventManager.SendDisplayWarningSound(false);

#if UNITY_IOS && !UNITY_EDITOR
                if (detector != null)
                {
                    if (detector.isMuted) {
                        EventManager.SendDisplayWarningSound(true);
                    }
                }
#endif
            }
        }

        if (neededTasks == 0) return;

        if (finishedTasks == neededTasks)
        {
            audioSource.PlayDelayed(waitForSeconds);
            neededTasks = 0;
            EventManager.SendSetAudioVarFor3DPlace(true);
        }
    }

    public void IncrementFinishedTasks()
    {
        finishedTasks++;
    }
}
