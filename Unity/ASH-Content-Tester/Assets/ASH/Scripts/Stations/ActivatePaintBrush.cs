using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePaintBrush : MonoBehaviour
{
    [SerializeField]
    GameObject paintBrush;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    private void EnablePaintBrush()
    {
        paintBrush.SetActive(!paintBrush.activeSelf);
    }

    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR += EnablePaintBrush;
    }

    void UnRegisterEvents()
    {
        EventManager.OnToggle3DPlaceAR -= EnablePaintBrush;
    }
    #endregion
}
