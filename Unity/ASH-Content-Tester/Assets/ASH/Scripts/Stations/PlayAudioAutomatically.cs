using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayAudioAutomatically : MonoBehaviour
{
    AudioSource audioOfStation;
    float secToWait = 3.0f;

    double clipLength;
    bool audioHasFinished = false;

    private MuteSwitchDetector detector;

    // Start is called before the first frame update
    void Start()
    {
        audioOfStation = GetComponent<AudioSource>();
        
        if (audioOfStation != null)
        {
            audioOfStation.PlayDelayed(secToWait);
            clipLength = audioOfStation.clip.length;
        }

#if UNITY_IOS && !UNITY_EDITOR
        detector = transform.parent.GetChild(1).GetComponent<MuteSwitchDetector>();
#endif
    }

    private void Update()
    {
        if (audioHasFinished) return;

        if (!audioOfStation.isPlaying)
        {
            if (audioOfStation.time == clipLength)
            {
                EventManager.SendSetAudioVarFor3DPlace(false);
                audioHasFinished = true;
            }
        }
        else
        {
            // Check if sound volume is muted
            if (AudioSettings.Mobile.muteState)
            {
                EventManager.SendDisplayWarningSound(true);
            }
            else
            {
                EventManager.SendDisplayWarningSound(false);

#if UNITY_IOS && !UNITY_EDITOR
                if (detector != null)
                {
                    if (detector.isMuted) {
                        EventManager.SendDisplayWarningSound(true);
                    }
                }
#endif
            }
        }
    }
}
