using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AnchorContent.Structures;
using System;
using ASH.Structures;


public class EventManager : MonoBehaviour
{ 

    #region CurrentlyActivePoi

    private static AppPointOfInterest _currentlyActivePoi = null;

    public static AppPointOfInterest CurrentlyActivePoi
    {
        get { return _currentlyActivePoi; }
        set
        {
            Debug.Log("CurrentlyActivePoi changed, new value= " + value.id);

            _currentlyActivePoi = value;

            if (OnCurrentlyActivePoiChanged != null)
            {
                OnCurrentlyActivePoiChanged(value);
            }
        }

    }
    public delegate void OnCurrentlyActivePoiChangedEvent(AppPointOfInterest poi);
    public static event OnCurrentlyActivePoiChangedEvent OnCurrentlyActivePoiChanged;

    #endregion

    #region CurrentlyActiveRoute

    private static Route _currentlyActiveRoute = null;

    public static Route CurrentlyActiveRoute
    {
        get { return _currentlyActiveRoute; }
        set
        {

            Debug.Log("CurrentlyActiveRoute changed, new value= " + value.id);

            _currentlyActiveRoute = value;

            if (OnCurrentlyActiveRouteChanged != null)
            {
                OnCurrentlyActiveRouteChanged(value);
            }
        }

    }
    public delegate void OnCurrentlyActiveRouteChangedEvent(Route route);
    public static event OnCurrentlyActiveRouteChangedEvent OnCurrentlyActiveRouteChanged;

    #endregion

    #region HamburgerMenuActiveState

    private static EHamburgerMenuState _hamburgerMenuActiveState = EHamburgerMenuState.None;

    public static EHamburgerMenuState HamburgerMenuActiveState
    {
        get { return _hamburgerMenuActiveState; }
        set
        {

            Debug.Log("HamburgerMenuActiveState changed, new value= " + value);

            _hamburgerMenuActiveState = value;

            if (OnHamburgerMenuActiveStateChanged != null)
            {
                OnHamburgerMenuActiveStateChanged(value);
            }

        }
    }

    public delegate void OnHamburgerMenuActiveStateChangedEvent(EHamburgerMenuState state);
    public static event OnHamburgerMenuActiveStateChangedEvent OnHamburgerMenuActiveStateChanged;

    #endregion

    #region CategoryHealth

    private static bool _categoryHealth = false;

    public static bool CategoryHealth
    {
        get { return _categoryHealth; }
        set
        {
            if (value != _categoryHealth)
            {
                Debug.Log("CategoryHealth changed, new value= " + value);

                _categoryHealth = value;

                if (OnCategoryHealthChanged != null)
                {
                    OnCategoryHealthChanged(value);
                }
            }
        }
    }

    public delegate void OnCategoryHealthChangedEvent(bool state);
    public static event OnCategoryHealthChangedEvent OnCategoryHealthChanged;

    #endregion

    #region CategorySport

    private static bool _categorySport = false;

    public static bool CategorySport
    {
        get { return _categorySport; }
        set
        {
            if (value != _categorySport)
            {
                Debug.Log("CategorySport changed, new value= " + value);

                _categorySport = value;

                if (OnCategorySportChanged != null)
                {
                    OnCategorySportChanged(value);
                }
            }
        }
    }

    public delegate void OnCategorySportChangedEvent(bool state);
    public static event OnCategorySportChangedEvent OnCategorySportChanged;

    #endregion

    #region CategoryArtAndCulture

    private static bool _categoryArtAndCulturep = false;

    public static bool CategoryArtAndCulture
    {
        get { return _categoryArtAndCulturep; }
        set
        {
            if (value != _categoryArtAndCulturep)
            {
                Debug.Log("CategoryArtAndCulture changed, new value= " + value);

                _categoryArtAndCulturep = value;

                if (OnCategoryArtAndCultureChanged != null)
                {
                    OnCategoryArtAndCultureChanged(value);
                }
            }
        }
    }

    public delegate void OnCategoryArtAndCultureChangedEvent(bool state);
    public static event OnCategoryArtAndCultureChangedEvent OnCategoryArtAndCultureChanged;

    #endregion

    #region RouteDataDownloaded
    public delegate void RouteDataDownloadedAction(bool downloaded);
    public static event RouteDataDownloadedAction OnRouteDataDownloaded;

    public static void RouteDataDownloaded(bool downloaded)
    {
        if (OnRouteDataDownloaded != null)
            OnRouteDataDownloaded(downloaded);
    }

    #endregion

    #region PointsOfInterestDownloaded
    public delegate void PointsOfInterestDownloadedAction(bool downloaded);
    public static event PointsOfInterestDownloadedAction OnPointsOfInterestDownloaded;

    public static void PointsOfInterestDownloaded(bool downloaded)
    {
        if (OnPointsOfInterestDownloaded != null)
            OnPointsOfInterestDownloaded(downloaded);
    }

    #endregion

    #region RouteDataProviderReady
    public delegate void RouteDataProviderReadyAction(bool downloaded);
    public static event RouteDataProviderReadyAction OnRouteDataProviderReady;

    public static void RouteDataProviderReady(bool downloaded)
    {
        if (OnRouteDataProviderReady != null)
            OnRouteDataProviderReady(downloaded);
    }

    #endregion

    #region Start Spawn Prefab
    public delegate void StartSpawnPrefabAction(string prefabName);
    public static event StartSpawnPrefabAction OnStartSpawnPrefab;


    public static void SendStartSpawnPrefab(string prefabName)
    {
        if (OnStartSpawnPrefab != null)
            OnStartSpawnPrefab(prefabName);
    }
    #endregion

    #region Cancel Spawn Prefab
    public delegate void CancelSpawnPrefabAction();
    public static event CancelSpawnPrefabAction OnCancelSpawnPrefab;


    public static void SendCancelSpawnPrefab()
    {
        if (OnCancelSpawnPrefab != null)
            OnCancelSpawnPrefab();
    }
    #endregion

    #region Select Contenet
    public delegate void SelectContentAction(GameObject content);
    public static event SelectContentAction OnSelectContent;


    public static void SendSelectContent(GameObject content)
    {
        if (OnSelectContent != null)
            OnSelectContent(content);
    }
    #endregion

    #region Remove Contenet
    public delegate void RemoveContentAction(GameObject content);
    public static event RemoveContentAction OnRemoveContent;


    public static void SendRemoveContent(GameObject content)
    {
        if (OnRemoveContent != null)
            OnRemoveContent(content);
    }
    #endregion

    #region Toggle Video Audio
    public delegate void ToggleVideoAudioAction();
    public static event ToggleVideoAudioAction OnToggleVideoAudio;


    public static void SendToggleVideoAudio()
    {
        if (OnToggleVideoAudio != null)
            OnToggleVideoAudio();
    }
    #endregion

    #region Change Icon of Button
    public delegate void ChangeButtonIconAction(Sprite icon);
    public static event ChangeButtonIconAction OnChangeButtonIcon;


    public static void SendChangeButtonIcon(Sprite icon)
    {
        if (OnChangeButtonIcon != null)
            OnChangeButtonIcon(icon);
    }
    #endregion

    #region Manual Placement
    public delegate void ManualPlacementAction();
    public static event ManualPlacementAction OnManualPlacement;


    public static void SendManualPlacement()
    {
        if (OnManualPlacement != null)
            OnManualPlacement();
    }
    #endregion

    #region Spatial Anchors

    #region StartAsaSession
    public delegate void StartAsaSessionAction(PointOfInterest pointOfInterest);
    public static event StartAsaSessionAction OnStartAsaSession;


    public static void SendStartAsaSession(PointOfInterest pointOfInterest)
    {
        if (OnStartAsaSession != null)
            OnStartAsaSession(pointOfInterest);
    }
    #endregion

    #region Restart
    public delegate void RestartAction();
    public static event RestartAction OnRestart;


    public static void SendRestart()
    {
        if (OnRestart != null)
            OnRestart();
    }
    #endregion

    #region Anchor Rescan
    public delegate void RescanAction();
    public static event RestartAction OnRescan;


    public static void SendRescan()
    {
        if (OnRescan != null)
            OnRescan();
    }
    #endregion

    #region Anchor Found
    public delegate void AnchorFoundAction();
    public static event AnchorFoundAction OnAnchorFound;


    public static void SendAnchorFound()
    {
        if (OnAnchorFound != null)
            OnAnchorFound();
    }
    #endregion

    #region SpawnModel
    public delegate void SpawnModelAction(GameObject anchor, ContentPlaced place);
    public static event SpawnModelAction OnSpawnModel;


    public static void SendSpawnModel(GameObject anchor, ContentPlaced place)
    {
        if (OnSpawnModel != null)
            OnSpawnModel(anchor, place);
    }
    #endregion

    #region ShowError
    public delegate void ShowErrorAction(string errorMessage);
    public static event ShowErrorAction OnShowError;

    public static void SendShowErrorr(string errorMessage)
    {
        if (OnShowError != null)
            OnShowError(errorMessage);
    }
    #endregion
    #endregion

    #region Open, Close, Refresh & Content AR
    #region Open AR
    public delegate void OpenARAction(PointOfInterest pointOfInterest);
    public static event OpenARAction OnOpenARAction;
    public static void SendOpenARAction(PointOfInterest pointOfInterest)
    {
        if (OnOpenARAction != null)
        {
           
            OnOpenARAction(pointOfInterest);

        }
    }
    #endregion
    #region Refresh AR
    public delegate void RefreshARAction();
    public static event RefreshARAction OnRefreshAR;
    public static void SendRefreshAR()
    {
        if (OnRefreshAR != null)
            OnRefreshAR();
    }
    #endregion
    #region Close AR
    public delegate void CloseARAction();
    public static event CloseARAction OnCloseARAction;
    public static void SendCloseARAction()
    {
        if (OnCloseARAction != null)
            OnCloseARAction();
    }
    #endregion

    #endregion

    #region AR Add Manual
    public delegate void Toggle3DPlaceARAction();
    public static event Toggle3DPlaceARAction OnToggle3DPlaceAR;
    public static void SendToggle3DPlaceAR()
    {
        if (OnToggle3DPlaceAR != null)
            OnToggle3DPlaceAR();
    }
    #endregion

    #region AR Set Audio Variable for 3D Manual Placement
    public delegate void SetAudioVarFor3DPlaceAction(bool val);
    public static event SetAudioVarFor3DPlaceAction OnSetAudioVarFor3DPlace;

    public static void SendSetAudioVarFor3DPlace(bool val)
    {
        if (OnSetAudioVarFor3DPlace != null)
            OnSetAudioVarFor3DPlace(val);
    }
    #endregion

    #region AR Warning Sound of Smartphone is muted
    public delegate void DisplayWarningMutedSoundAction(bool val);
    public static event DisplayWarningMutedSoundAction OnDisplayWarningSound;

    public static void SendDisplayWarningSound(bool val)
    {
        if (OnDisplayWarningSound != null)
            OnDisplayWarningSound(val);
    }
    #endregion
}




