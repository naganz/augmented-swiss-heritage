
using Mapbox.Unity.Location;
using Mapbox.Unity.Map;
using UnityEngine;

public class YouAreHereLocationController : MonoBehaviour
{

    #region Inspector Properties

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    private ILocationProvider _locationProvider;
    private ILocationProvider locationProvider
    {
        get
        {
            if (_locationProvider == null)
            {
                _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
            }

            return _locationProvider;
        }
    }

    private AbstractMap _map;
    private AbstractMap map
    {
        get
        {
            if (_map == null)
            {
                _map = LocationProviderFactory.Instance.mapManager;
            }

            return _map;
        }
    }

    private bool onLocationUpdatedEventAdded = false;

    #endregion

    #region Framework Functions

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    void Update()
    {
        if (!onLocationUpdatedEventAdded && locationProvider != null)
        {
            this.subscribeToEvents();
            onLocationUpdatedEventAdded = true;
        }
    }

    #endregion

    #region Events

    private void subscribeToEvents()
    {
        locationProvider.OnLocationUpdated += onLocationUpdated;
        map.OnUpdated += onMapUpdated;
    }

    private void unsubscribeFromEvents()
    {
        if (locationProvider != null)
        {
            locationProvider.OnLocationUpdated -= onLocationUpdated;
        }
        if (map != null)
        {
            map.OnUpdated -= onMapUpdated;
        }
        onLocationUpdatedEventAdded = false;
    }

    private void onLocationUpdated(Location obj)
    {
        Debug.Log("YouAreHere onLocationUpdated");
        updateMapLocation();
    }

    private void onMapUpdated()
    {
        updateMapLocation();
    }

    #endregion

    #region Public Functions

    #endregion

    #region Private Functions

    private void updateMapLocation()
    {
        if (locationProvider != null && map != null)
        {
            transform.localPosition = map.GeoToWorldPosition(locationProvider.CurrentLocation.LatitudeLongitude);
        }
    }

    #endregion
}
