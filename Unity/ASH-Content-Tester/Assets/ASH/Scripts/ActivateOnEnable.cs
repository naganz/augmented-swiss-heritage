using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnEnable : MonoBehaviour
{


    [SerializeField]
    GameObject[] toActivate;
    [SerializeField]
    GameObject[] toDeactivate;
    private void OnEnable()
    {
        if (toActivate != null)
        {
            foreach (var go in toActivate)
            {
                go.SetActive(true);
            }
        }
        
        if (toDeactivate != null)
        {
            foreach (var go in toDeactivate)
            {
                go.SetActive(false);
            }
        }
        
    }
}
