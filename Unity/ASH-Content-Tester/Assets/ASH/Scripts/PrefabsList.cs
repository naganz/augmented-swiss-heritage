using ASH.Structures;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public enum ERouteId
{
    route_1 = 0,
    route_2 = 10,
    route_3 = 20,
    route_4 = 30,
}

[Serializable]
public struct PrefabsListItem 
{
    public string prefabName;

    public EPOIType type;

    public Coordinates coordinates;

    public ERouteId route;

}

[Serializable]
public struct Coordinates
{
    public float lat;
    public float lng;
}


[Serializable]
public struct RoutListItem
{
    public string routeName;
    public ERouteId routeId;
}

public class PrefabsList : MonoBehaviour
{
    [SerializeField]
    RoutListItem[] routeList;

    [SerializeField]
    PrefabsListItem[] prefabsList;

    Dropdown dropdown;

    void Start()
    {
        dropdown = this.gameObject.GetComponentInParent<Dropdown>();
        dropdown.options = new List<Dropdown.OptionData>();
        foreach(PrefabsListItem item in prefabsList)
        {
            dropdown.options.Add(new Dropdown.OptionData(item.prefabName));
        }
    }
}

