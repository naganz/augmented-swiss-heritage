using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RouteTitleController : MonoBehaviour
{
    #region Inspector Properties

    [SerializeField]
    private Text textRoute_Title;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Framework Functions
    void OnEnable()
    {
        this.subscribeToEvents();
    }

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events
    private void subscribeToEvents()
    {
        EventManager.OnCurrentlyActiveRouteChanged += onCurrentlyActiveRouteChanged;
    }

    private void unsubscribeFromEvents()
    {
        EventManager.OnCurrentlyActiveRouteChanged -= onCurrentlyActiveRouteChanged;

    }
    private void onCurrentlyActiveRouteChanged(ASH.Structures.Route route)
    {
        this.updateRouteTitle();

    }

    #endregion

    #region Public Functions

    #endregion

    #region Private Functions

    private void updateRouteTitle()
    {
        this.textRoute_Title.text = EventManager.CurrentlyActiveRoute.Name;
    }

    #endregion
}