using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;

/// <summary>
/// Obsolete, bitte nicht mehr verwenden
/// </summary>
[Obsolete("Not used any more", true)]
public class MenuController : MonoBehaviour
{
    [SerializeField]
    GameObject AddNewPanel;
    
    [SerializeField]
    GameObject CancelAddNewPanel;

    [SerializeField]
    GameObject SelectedContentPanel;

    [SerializeField]
    Dropdown SelectedPrefab;

    GameObject selectedContent;

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion

    #region User Action
    
    public void AddNewPrefab(string name)
    {
        EventManager.SendStartSpawnPrefab(name);
        hideAllPanels();
        CancelAddNewPanel.SetActive(true);
    }
    public void CancelAddNewPrefab()
    {
        EventManager.SendCancelSpawnPrefab();
        hideAllPanels();
        //AddNewPanel.SetActive(true);
    }

    public void DeleteContent()
    {
        EventManager.SendRemoveContent(this.selectedContent);
        hideAllPanels();
        //AddNewPanel.SetActive(true);
    }

   
    //public void PlaceContentManually()
    //{
    //    EventManager.SendManualPlacement();
    //}

    void hideAllPanels()
    {
        AddNewPanel.SetActive(false);
        CancelAddNewPanel.SetActive(false);
        SelectedContentPanel.SetActive(false);
        
    }

    //public void StartAsaSession()
    //{
    //    EventManager.SendStartAsaSession();
    //}
    #endregion //User Action

    #region Events
    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnSelectContent += EventManager_OnSelectContent; ;
       
    }

    

    void UnRegisterEvents()
    {
        EventManager.OnSelectContent -= EventManager_OnSelectContent; ;
       

    }
    #endregion

    #region Handle Events
    private void EventManager_OnSelectContent(GameObject content)
    {
        if (this.selectedContent == content)
        {
            this.selectedContent = null;
            hideAllPanels();
            //AddNewPanel.SetActive(true);
        }
        else
        {
            this.selectedContent = content;
            hideAllPanels();
            SelectedContentPanel.SetActive(true);
        }
       
    }
    
    #endregion //Handle Events

    #endregion // Events
}
