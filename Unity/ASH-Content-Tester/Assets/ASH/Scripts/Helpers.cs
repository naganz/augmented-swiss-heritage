using ASH.Structures;
using Mapbox.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EHamburgerMenuState
{
	None = -1,
	Intro = 0,
	Routen = 10,
	RoutenDetail = 11,
	Tutorial = 20,
	Impressum = 30,
	StationenAlle = 40,
	StationenDetail = 41,
	Lesezeichen = 50,
}

public class AppPointOfInterest : PointOfInterest
{
	public AppPointOfInterest(PointOfInterest poi, Sprite sprite)
	{
		this.Description = poi.Description;
		this.id = poi.id;
		this.location = poi.location;
		this.Name = poi.Name;
		this.PoiTypes = poi.PoiTypes;
		this.RouteId = poi.RouteId;
		this.TenantId = poi.TenantId;
		this.Sprite = sprite;
		this.XrIntroText = poi.XrIntroText;
	}
	public Sprite Sprite { get; set; }
}


public static class Helpers
{
	public static void DestroyAllChildren(GameObject target)
	{
		foreach (Transform child in target.GetComponentsInChildren<Transform>(true))
		{
			if (child.gameObject != target)
			{
				GameObject.Destroy(child.gameObject);
			}
		}
	}

	public static string FormatDistance(double distance, int maxDistance) 
	{
		string retval;
		if (distance == double.MaxValue) 
		{
			return "-";
		}
		if (distance < maxDistance)
		{
			if (distance <= 1000)
			{
				retval = string.Format("{0:F0} m", distance);
			}
			else
			{
				retval = string.Format("{0:F1} km", distance / 1000);
			}
		}
		else
		{
			retval = string.Format("> {0:F1} km", maxDistance / 1000);
		}
		return retval;
	}

	public static double GetGeoDistance(Vector2d a, Vector2d b)
	{
		if (double.IsNaN(a.x) || double.IsNaN(a.y) || double.IsNaN(b.x) || double.IsNaN(b.y))
		{
			throw new ArgumentException("Argument_LatitudeOrLongitudeIsNotANumber");
		}
		else
		{
			double latitude = a.x * 0.0174532925199433;
			double longitude = a.y * 0.0174532925199433;
			double num = b.x * 0.0174532925199433;
			double longitude1 = b.y * 0.0174532925199433;
			double num1 = longitude1 - longitude;
			double num2 = num - latitude;
			double num3 = Math.Pow(Math.Sin(num2 / 2), 2) + Math.Cos(latitude) * Math.Cos(num) * Math.Pow(Math.Sin(num1 / 2), 2);
			double num4 = 2 * Math.Atan2(Math.Sqrt(num3), Math.Sqrt(1 - num3));
			double num5 = 6376500 * num4;
			return num5;
		}
	}
	public static bool IsActivePoi(PointOfInterest poi, bool searchActiveRouteOnly)
	{
		if (poi != null && EventManager.CurrentlyActiveRoute != null)
		{
			if (!searchActiveRouteOnly || poi.RouteId == EventManager.CurrentlyActiveRoute.id)
			{
				foreach (EPOIType poiType in poi.PoiTypes)
				{
					if (poiType == EPOIType.Gesundheit && EventManager.CategoryHealth)
					{
						return true;
					}
					else if (poiType == EPOIType.Kunst_und_Kultur && EventManager.CategoryArtAndCulture)
					{
						return true;
					}
					else if (poiType == EPOIType.Sport && EventManager.CategorySport)
					{
						return true;
					}
				}
			}
		}
		return false;
	}
}
