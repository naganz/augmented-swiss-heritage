using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;


public class ARSpawner : MonoBehaviour
{
    [SerializeField]
    ARRaycastManager m_RaycastManager;

    [SerializeField]
    [Tooltip("Container for Content with manipulation Scripts")]
    GameObject contentContainerPrefab;

    [SerializeField]
    [Tooltip("AR Indicator to place content")]
    GameObject arIndicatorPrefab;

    bool isRayActive;
    GameObject prefab;
    GameObject arIndicator;
    List<ARRaycastHit> m_Hits = new List<ARRaycastHit>();
 

    #region Unity Lifecycle
    private void OnEnable()
    {
        RegisterEvents();
    }
    void OnDisable()
    {
        UnRegisterEvents();
    }
    #endregion
    void Update()
    {
        if (isRayActive)
        {
            var pos = new Vector2(Screen.width / 2, Screen.height / 2);
            if (m_RaycastManager.Raycast(pos, m_Hits))
            {
                var pose1 = m_Hits[0].pose;

                if (Input.touchCount == 0)
                {
                    if (arIndicator == null)
                    {
                        arIndicator = Instantiate(arIndicatorPrefab, this.transform);
                    }
                    arIndicator.transform.position = pose1.position;
                }
                else
                {
                    if (m_Hits.Count > 0 && isInRaycastZone(Input.GetTouch(0).position))
                    {
                        if (prefab == null)
                        {
                            Debug.LogWarning("ARSpawner.Update: Raycast is on, but no prefabName is set");
                        }
                        else
                        {
                            // remove AR Indicator
                            Destroy(arIndicator);
                            arIndicator = null;

                            // Rotation zum Benutzer
                            Vector3 toV3 = Camera.main.transform.position;
                            toV3.y = pose1.position.y;
                            Vector3 relativePos = toV3 - pose1.position;
                            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
                            rotation *= Quaternion.Euler(0f, 180f, 0f);

                            var container = Instantiate(this.contentContainerPrefab, this.gameObject.transform);
                            
                            container.transform.rotation = rotation;
                            var inst = Instantiate(prefab, container.transform);

                            container.transform.position = pose1.position;
                            EventManager.SendSelectContent(container);
                            isRayActive = false;
                            EventManager.SendManualPlacement();
                        }

                    }

                }

            }
        }

    }

    

    // No Reycast on buttom of the screen
    private bool isInRaycastZone(Vector2 position)
    {
        bool noRay = position.y < 100;

        return !noRay;
    }

    #region Events
    #region Register/Unregister Events
    void RegisterEvents()
    {
        EventManager.OnStartSpawnPrefab += EventManager_OnStartSpawnPrefab;
        EventManager.OnCancelSpawnPrefab += EventManager_OnCancelSpawnPrefab;
        EventManager.OnRemoveContent += EventManager_OnRemoveContent;
        EventManager.OnSpawnModel += EventManager_OnSpawnModel;
        EventManager.OnCloseARAction += EventManager_OnCloseARAction;
    }

   

    void UnRegisterEvents()
    {
        EventManager.OnStartSpawnPrefab -= EventManager_OnStartSpawnPrefab;
        EventManager.OnCancelSpawnPrefab -= EventManager_OnCancelSpawnPrefab;
        EventManager.OnRemoveContent -= EventManager_OnRemoveContent;
        EventManager.OnSpawnModel -= EventManager_OnSpawnModel;
    }
    #endregion

    #region Handle Events
    private void EventManager_OnStartSpawnPrefab(string prefabName)
    {
        var aoh = Addressables.LoadAssetAsync<GameObject>(prefabName);
        aoh.Completed += (handler) => {
            prefab = handler.Result;
            isRayActive = true;
        };
    }
    private void EventManager_OnCancelSpawnPrefab()
    {
        this.prefab = null;
        isRayActive = false;
    } 

    private void EventManager_OnRemoveContent(GameObject content)
    {
        Destroy(content);
    }

    private void EventManager_OnSpawnModel(GameObject anchor, AnchorContent.Structures.ContentPlaced place)
    {
        var aoh = Addressables.LoadAssetAsync<GameObject>(place.Prefab);
        aoh.Completed += (handler) => {

            prefab = handler.Result;
            anchor.transform.SetParent(this.gameObject.transform);
            var container = Instantiate(this.contentContainerPrefab, anchor.transform);
            var inst = Instantiate(prefab, container.transform);
            
            container.transform.localPosition = new Vector3(place.Position.X, place.Position.Y, place.Position.Z);
            container.transform.localRotation = new Quaternion(place.Rotation.X, place.Rotation.Y, place.Rotation.Z, place.Rotation.W);
            // Scale only if Scale > 0
            if ((place.Scale.X + place.Scale.Y + place.Scale.Z) > 0)
            {
                container.transform.localScale = new Vector3(place.Scale.X, place.Scale.Y, place.Scale.Z);
            }
            EventManager.SendSelectContent(container);
        };
    }
    private void EventManager_OnCloseARAction()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
    #endregion //Handle Events
    #endregion
}
