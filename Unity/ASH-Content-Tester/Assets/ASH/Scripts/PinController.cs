using ASH.Structures;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class PinController : MonoBehaviour
{

    #region Inspector Properties

    [Header("Scene Objects")]
    [SerializeField]
    private GameObject content;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [Header("Config Values")]
    [SerializeField]
    float poiScale = 100f;

    [SerializeField]
    [Range(-1, 1)]
    private float growValue;

    [SerializeField]
    private EHamburgerMenuState menuStateOnClick;
    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    private PointOfInterest poi;
    private AbstractMap map;
    private Sprite sprite;
    private AsyncOperationHandle<Sprite> handle;



    #endregion

    #region Framework Functions

    private void OnEnable()
    {
        this.subscribeToEvents();
        content.SetActive(Helpers.IsActivePoi(this.poi, true));
    }

    private void OnDisable()
    {

        this.unsubscribeFromEvents();
    }

    private void Update()
    {
        Vector2d location = new Vector2d(poi.location.coordinates[1], poi.location.coordinates[0]);
        this.transform.localPosition = map.GeoToWorldPosition(location, true);
        this.transform.localScale = new Vector3(poiScale, poiScale, poiScale);
    }

    private void OnDestroy()
    {
        if (sprite != null)
        {
            Addressables.Release(this.handle);
        }
    }

    #endregion

    #region Events
    private void subscribeToEvents()
    {
        EventManager.OnCategoryHealthChanged += onCategoryHealthChanged;
        EventManager.OnCategorySportChanged += onCategorySportChanged;
        EventManager.OnCategoryArtAndCultureChanged += onCategoryArtAndCultureChanged;
        EventManager.OnCurrentlyActivePoiChanged += onCurrentlyActivePoiChanged;
        EventManager.OnCurrentlyActiveRouteChanged += onCurrentlyActiveRouteChanged;
    }

    private void unsubscribeFromEvents()
    {
        EventManager.OnCategoryHealthChanged -= onCategoryHealthChanged;
        EventManager.OnCategorySportChanged -= onCategorySportChanged;
        EventManager.OnCategoryArtAndCultureChanged -= onCategoryArtAndCultureChanged;
        EventManager.OnCurrentlyActivePoiChanged -= onCurrentlyActivePoiChanged;
        EventManager.OnCurrentlyActiveRouteChanged -= onCurrentlyActiveRouteChanged;
    }

    private void onCurrentlyActiveRouteChanged(Route route)
    {
        content.SetActive(Helpers.IsActivePoi(this.poi, true));
    }

    private void onCurrentlyActivePoiChanged(PointOfInterest poi)
    {
        Vector3 size = Vector3.one;
        if (this.poi.id == poi.id)
        {
            size = new Vector3(1 + growValue, 1 + growValue, 1 + growValue);
        }
        content.transform.localScale = size;
    }

    private void onCategoryHealthChanged(bool state)
    {
        content.SetActive(Helpers.IsActivePoi(this.poi, true));
    }

    private void onCategorySportChanged(bool state)
    {
        content.SetActive(Helpers.IsActivePoi(this.poi, true));

    }

    private void onCategoryArtAndCultureChanged(bool state)
    {
        content.SetActive(Helpers.IsActivePoi(this.poi, true));

    }

    #endregion

    #region Public Functions

    public void HandleButtonClick()
    {
        EventManager.HamburgerMenuActiveState = this.menuStateOnClick;
        AppPointOfInterest currentlyActivePoi = new AppPointOfInterest(this.poi, this.sprite);
        EventManager.CurrentlyActivePoi = currentlyActivePoi;
    }

    public void HandleNoButtonClick()
    {
        content.transform.localScale = Vector3.one;
        //EventManager.CurrentlyActivePoi = null;
    }

    public void Init(PointOfInterest poi, AbstractMap map)
    {
        this.poi = poi;
        this.map = map;

        string addressableName = poi.TenantId + "_" + poi.id + "_Pin";

        Addressables.InitializeAsync().Completed += (handle) => {
            Debug.Log(handle.Status);
            Addressables.LoadAssetAsync<Sprite>(addressableName).Completed += loadingPinDone;
        };
    }



    #endregion

    #region Private Functions

    private void loadingPinDone(AsyncOperationHandle<Sprite> handle)
    {
        this.handle = handle;
        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            Sprite sprite = handle.Result;
            if (sprite != null)
            {
                this.spriteRenderer.sprite = sprite;
                this.sprite = sprite;
            }
        }
        else if (handle.Status == AsyncOperationStatus.Failed)
        {
            Debug.Log(handle.DebugName + " sprite could not be loaded.");
        }
    }

    #endregion

}
