using ASH.Structures;
using MagneticScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class RoutesSpawner : MonoBehaviour
{

    #region Inspector Properties

    [SerializeField]
    GameObject routePrefab;

    [SerializeField]
    Transform routeContainer;

    [SerializeField]
    GameObject magneticScrollView;

    [SerializeField]
    float arrangingDelay;

    [SerializeField]
    Text textRoute_Title;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    MagneticScrollRect msr;

    #endregion

    #region Framework Functions

    void OnEnable()
    {
        msr = magneticScrollView.GetComponent<MagneticScrollRect>();
        this.subscribeToEvents();
    }

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events
    private void subscribeToEvents()
    {
        EventManager.OnRouteDataProviderReady += this.onRouteDataProviderReady;

    }

    private void unsubscribeFromEvents()
    {
        EventManager.OnRouteDataProviderReady -= this.onRouteDataProviderReady;

    }

    private void onRouteDataProviderReady(bool ready)
    {
        if (ready)
        {
            spawnRoutes();
        }
    }

    public void OnRouteChange()
    {
        EventManager.CurrentlyActiveRoute = msr.CurrentSelectedObject.GetComponent<RoutePanelController>().Route;
    }


    #endregion

    #region Public Functions

    #endregion

    #region Private Functions

    private async void spawnRoutes()
    {
        msr.StartAutoArranging();
        foreach (Route route in RouteDataProvider.Instance.Routes)
        {
            GameObject instance = Instantiate(routePrefab);
            RoutePanelController rpc = instance.GetComponent<RoutePanelController>();
            rpc.init(route);

            instance.transform.localPosition = Vector3.zero;
            instance.transform.localScale = Vector3.one;
            instance.transform.SetParent(this.routeContainer);

            RectTransform rectTrans = instance.GetComponent<RectTransform>();
            rectTrans.localScale = Vector3.one;
            rectTrans.localPosition = Vector3.zero;
        }
        await Task.Delay(TimeSpan.FromSeconds(arrangingDelay));
        msr.StopAutoArranging();
    }

    #endregion
}
