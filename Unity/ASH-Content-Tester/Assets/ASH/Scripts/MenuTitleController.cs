using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct EnumTranslation
{
    public string name;
    public List<EHamburgerMenuState> hamburgerMenuStates;
}

public class MenuTitleController : MonoBehaviour
{
    #region Inspector Properties

    [SerializeField]
    private Text textMenu_Title;

    [SerializeField]
    private string DefaultMenu_Title;

    [SerializeField]
    private EnumTranslation[] enumTranslations;
    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Framework Functions
    void OnEnable()
    {
        this.subscribeToEvents();
    }

    void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    #endregion

    #region Events
    private void subscribeToEvents()
    {
        EventManager.OnHamburgerMenuActiveStateChanged += onHamburgerMenuActiveStateChanged;
    }

    private void unsubscribeFromEvents()
    {
        EventManager.OnHamburgerMenuActiveStateChanged -= onHamburgerMenuActiveStateChanged;
    }
    private void onHamburgerMenuActiveStateChanged(EHamburgerMenuState e)
    {
            updateTextMenu_Title();
    }

    #endregion

    #region Public Functions

    #endregion

    #region Private Functions

    private void updateTextMenu_Title()
    {
        textMenu_Title.text = DefaultMenu_Title;
        foreach (EnumTranslation enumTranslation in enumTranslations) 
        {
            if (enumTranslation.hamburgerMenuStates.Contains(EventManager.HamburgerMenuActiveState)) 
            {
                this.textMenu_Title.text = enumTranslation.name;
                break;
            }
        }
    }

    #endregion
}
