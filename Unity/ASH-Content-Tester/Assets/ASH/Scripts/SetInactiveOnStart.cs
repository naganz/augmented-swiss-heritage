﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetInactiveOnStart : MonoBehaviour
{
    [SerializeField]
    bool IsActiveOnStart;

    
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(IsActiveOnStart);
    }

    
}
