using ASH.Structures;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class StationPanelController : MonoBehaviour
{

    #region Inspector Properties

    [SerializeField]
    GameObject content;

    [SerializeField]
    TextMeshProUGUI title;

    [SerializeField]
    TextMeshProUGUI category;

    [SerializeField]
    private Image image;

    [SerializeField]
    private EHamburgerMenuState menuStateOnClick;
    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    private PointOfInterest poi;
    private Sprite sprite;
    private AsyncOperationHandle<Sprite> handle;

    #endregion

    #region Framework Functions
    private void OnEnable()
    {
        this.subscribeToEvents();
        content.SetActive(isActive());
    }

    private void OnDisable()
    {
        this.unsubscribeFromEvents();
    }

    private void OnDestroy()
    {
        if (this.image != null)
        {
            Addressables.Release(handle);
        }
    }

    #endregion

    #region Events
    private void subscribeToEvents()
    {
        EventManager.OnCategoryHealthChanged += onCategoryHealthChanged;
        EventManager.OnCategorySportChanged += onCategorySportChanged;
        EventManager.OnCategoryArtAndCultureChanged += onCategoryArtAndCultureChanged;
    }

    private void unsubscribeFromEvents()
    {
        EventManager.OnCategoryHealthChanged -= onCategoryHealthChanged;
        EventManager.OnCategorySportChanged -= onCategorySportChanged;
        EventManager.OnCategoryArtAndCultureChanged -= onCategoryArtAndCultureChanged;
    }


    private void onCategoryHealthChanged(bool state)
    {
        content.SetActive(isActive());
    }

    private void onCategorySportChanged(bool state)
    {
        content.SetActive(isActive());

    }

    private void onCategoryArtAndCultureChanged(bool state)
    {
        content.SetActive(isActive());

    }

    #endregion

    #region Public Functions

    public void Init(PointOfInterest poi)
    {
        this.poi = poi.DeepCopy();
        this.title.text = poi.Name;
        this.category.text = poi.PoiTypes[0].ToString().Replace("_", " ");

        string addressableName = poi.TenantId + "_" + poi.id + "_Panel";

        Addressables.InitializeAsync().Completed += (handle) => {
            Debug.Log(handle.Status);
            Addressables.LoadAssetAsync<Sprite>(addressableName).Completed += loadingPanelDone;
        };
    }

    public void HandleARClick()
    {
        //Start AR Session with POI info
        // string addressableName = poi.TenantId + "_" + poi.id + "_AR";
        EventManager.SendOpenARAction(poi);
    }

    public void HandleButtonClick()
    {
        EventManager.HamburgerMenuActiveState = this.menuStateOnClick;
        AppPointOfInterest currentlyActivePoi = new AppPointOfInterest(this.poi, this.sprite);
        EventManager.CurrentlyActivePoi = currentlyActivePoi;
        EventManager.CurrentlyActiveRoute = RouteDataProvider.Instance.GetRoute(currentlyActivePoi.RouteId);
    }
    #endregion

    #region Private Functions
    private void loadingPanelDone(AsyncOperationHandle<Sprite> handle)
    {
        this.handle = handle;
        if (handle.Status == AsyncOperationStatus.Succeeded)
        {
            Sprite _image = handle.Result;
            if (_image != null)
            {
                this.image.sprite = _image;
            }
        }
        else if (handle.Status == AsyncOperationStatus.Failed)
        {
            Debug.Log(handle.DebugName + " sprite could not be loaded.");
        }
    }



    private bool isActive()
    {
        if (poi != null)
        {
            foreach (EPOIType poiType in poi.PoiTypes)
            {
                if (poiType == EPOIType.Gesundheit && EventManager.CategoryHealth)
                {
                    return true;
                }
                else if (poiType == EPOIType.Kunst_und_Kultur && EventManager.CategoryArtAndCulture)
                {
                    return true;
                }
                else if (poiType == EPOIType.Sport && EventManager.CategorySport)
                {
                    return true;
                }
            }
        }
        return false;

    }

    #endregion

}
