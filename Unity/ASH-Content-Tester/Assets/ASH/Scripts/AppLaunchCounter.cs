using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppLaunchCounter : MonoBehaviour
{

    [SerializeField]
    GameObject Intro;

    [SerializeField]
    GameObject IntroStartButton;

    [SerializeField]
    GameObject Tutorial;

    private void Start()
    {

        if (!PlayerPrefs.HasKey("Tutorial"))
        {

            Intro.SetActive(true);
            IntroStartButton.SetActive(true);
            Tutorial.SetActive(true);
            PlayerPrefs.SetInt("Tutorial", 1);
        }

    }
        
    
}


