using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FilterController : MonoBehaviour
{

    #region Inspector Properties

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Framework Functions

    #endregion

    #region Events
    private void Start()
    {
        this.HandleClick_CategoryArtAndCulture(true);
        this.HandleClick_CategoryHealth(true);
        this.HandleClick_CategorySport(true);
    }
    #endregion

    #region Public Functions

    public void HandleClick_CategorySport(bool state)
    {
        EventManager.CategorySport = state;
    }
    public void HandleClick_CategoryHealth(bool state)
    {
        EventManager.CategoryHealth = state;
    }
    public void HandleClick_CategoryArtAndCulture(bool state)
    {
        EventManager.CategoryArtAndCulture = state;
    }

    #endregion

    #region Private Functions

    #endregion

}
