﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASH.Structures
{
    public class PointOfInterest
    {
        public string id { get; set; }
        public string TenantId { get; set; } = "Davos";
        public string RouteId { get; set; }
        public string PrefabName { get { return baseName + "_AR"; } }
        public string PinName { get { return baseName + "_Pin"; } }
        public string XrIntroImage { get { return baseName + "_XrIntro"; } }
        public string PanelName { get { return baseName + "_Panel"; } }
        public string Name { get; set; }
        public string Description { get; set; }
        public string XrIntroText { get; set; }
        public List<EPOIType> PoiTypes;
        public LocationPoint location { get; set; }

        private string baseName
        {
            get { return TenantId + "_" + id; }
        }

        public PointOfInterest DeepCopy()
        {
            PointOfInterest temp = (PointOfInterest)this.MemberwiseClone();
            temp.PoiTypes = new List<EPOIType>(this.PoiTypes);
            temp.location = (this.location == null)?null:new LocationPoint() { coordinates = this.location.coordinates, type = this.location.type };
            return temp;
        }
    }
}
