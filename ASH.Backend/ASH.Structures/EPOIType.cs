﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASH.Structures
{
    public enum EPOIType
    {
        Gesundheit = 10,
        Sport = 20,
        Kunst_und_Kultur = 30
    }
}
