﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ASH.Structures
{
    public class LocationPoint
    {
        public string type { get; set; } = "Point";
        public float[] coordinates { get; set; } = new float[2];

        public LocationPoint Clone()
        {
            return new LocationPoint
            {
                type = type,
                coordinates = coordinates.ToArray()
            };
        }
    }
}
