﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASH.Structures
{
    public class Route
    {
        public string id { get; set; }
        public string TenantId { get; set; } = "Davos";
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
