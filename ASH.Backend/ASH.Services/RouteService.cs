using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Documents.Client;
using ASH.Structures;
using System.Linq;

namespace ASH.Services
{
    public static class RouteService
    {
        [FunctionName("Route-List")]
        public static IActionResult RunPointOfInterestListGet(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "{tenantid}/routes")] HttpRequest req,
             string tenantid,
            [CosmosDB(
                databaseName: "ash-db",
                collectionName: "Route",
                ConnectionStringSetting = "CosmosDBConnection")] DocumentClient expClient,
            ILogger log)
        {

            if (string.IsNullOrWhiteSpace(tenantid)) return new BadRequestObjectResult("Parameter 'tenantid' fehlt");

            Uri collectionUri = UriFactory.CreateDocumentCollectionUri("ash-db", "Route");
            var query = expClient.CreateDocumentQuery<Route>(collectionUri)
            .AsEnumerable<Route>();

            return new OkObjectResult(query);
        }
    }
}
