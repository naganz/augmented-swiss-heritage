using System;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Text.Json;
using Microsoft.Azure.Documents.Client;
using ASH.Structures;

namespace ASH.Services
{
    public static class PointOfInterestService
    {
        [FunctionName("PointOfInterest-List")]
        public static IActionResult RunPointOfInterestListGet(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "{tenantid}/pointofinterests")] HttpRequest req,
             string tenantid,
            [CosmosDB(
                databaseName: "ash-db",
                collectionName: "PointOfInterest",
                ConnectionStringSetting = "CosmosDBConnection")] DocumentClient expClient,
            ILogger log)
        {

            if (string.IsNullOrWhiteSpace(tenantid)) return new BadRequestObjectResult("Parameter 'tenantid' fehlt");

            Uri collectionUri = UriFactory.CreateDocumentCollectionUri("ash-db", "PointOfInterest");
            var query = expClient.CreateDocumentQuery<PointOfInterest>(collectionUri)
            .AsEnumerable<PointOfInterest>();

            return new OkObjectResult(query);
        }
    }
}
